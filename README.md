# Retro editor playground

This is my personal playground editor. At the moment it's not intended to work
on any machines other than my own or have any features beyond what I use.

Thanks to Autumn Lamonte for [jexer](https://gitlab.com/klamonte/jexer) and
especially for the excellent [porting
notes](https://gitlab.com/klamonte/jexer/-/wikis/porting).

See the [LICENSE.txt](LICENSE.txt) file for information on copying.
