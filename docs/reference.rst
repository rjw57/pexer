Reference
=========

.. automodule:: pexer

Application and event loop
--------------------------

.. autoclass:: pexer.TApplication

Widgets
-------

.. autoclass:: pexer.TWidget

.. autoclass:: pexer.TFill

.. autoclass:: pexer.TDesktop

Containers
``````````

.. autoclass:: pexer.TBox

Windows
```````

.. autoclass:: pexer.TWindow

.. autoclass:: pexer.TWindowStack

Data structures
---------------

.. autoclass:: pexer.TBoxDirection

Size
````

.. autoclass:: pexer.TSize

.. autoclass:: pexer.TOptionalSize

.. autoclass:: pexer.TPoint

Events
------

.. autoclass:: pexer.event.TInputEvent

Keyboard Events
```````````````

.. autoclass:: pexer.event.TKeypressEvent

.. autoclass:: pexer.TKeypress

Mouse Events
````````````

.. autoclass:: pexer.event.TMouseEvent

Resize Events
`````````````

.. autoclass:: pexer.event.TResizeEvent

Command Events
``````````````

.. autoclass:: pexer.event.TCommandEvent

.. autoclass:: pexer.TCommand

.. automodule:: pexer.commands

Low-level API
-------------

Text Cells
``````````

.. autoclass:: pexer.bits.Cell

.. autoclass:: pexer.bits.CellFlags

.. autoclass:: pexer.bits.CellAttributes

Colors and theming
``````````````````

.. autoclass:: pexer.bits.Color

.. autoclass:: pexer.bits.ColorTheme

