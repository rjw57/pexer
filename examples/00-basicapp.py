"""
Basic application which starts, waits for a moment and then exits.

"""
import asyncio

import pexer
import pexer.commands


class App(pexer.TApplication):
    def on_start(self):
        self.call_later(self.wait_then_exit)

    async def wait_then_exit(self):
        await asyncio.sleep(3)
        self.post_command(pexer.commands.EXIT)


if __name__ == '__main__':
    App().run()
