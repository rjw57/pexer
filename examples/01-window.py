"""
Basic application which starts, waits for a moment and then exits.

"""
import asyncio

import pexer
import pexer.commands


class App(pexer.TApplication):
    def on_start(self):
        self.add_window(pexer.TWindow(
            position=pexer.TPoint(4, 2), size=pexer.TSize(50, 10),
            title="Testing 1"
        ))
        self._win = pexer.TWindow(
            position=pexer.TPoint(8, 4), size=pexer.TSize(50, 12),
            title="Testing 2", content=pexer.TFill(
                fill_character='X', fill_attributes='twindow.background'
            )
        )
        self.add_window(self._win)
        self.add_window(pexer.TWindow(
            position=pexer.TPoint(10, 6), size=pexer.TSize(30, 8),
            title="Testing 3"
        ))
        self.call_later(self.loop)

    async def loop(self):
        for tx in range(-50, 80):
            self._win.set_position(pexer.TPoint(tx, 4))
            self.do_repaint()
            await asyncio.sleep(1./30.)
        self.post_command(pexer.commands.EXIT)


if __name__ == '__main__':
    App().run()
