"""
Basic application which starts, waits for a moment and then exits.

"""
import asyncio

import pexer
import pexer.commands
import pexer.event


class QuitWidget(pexer.TFill):
    def __init__(self, **kwargs):
        super().__init__(fill_character="O", fill_attributes="twindow.background", **kwargs)

    def on_mouse(self, event: pexer.event.TMouseEvent) -> bool:
        if self.application is None:
            return False

        if event.type == pexer.event.TMouseEvent.Type.MOUSE_UP and event.mouse1:
            self.application.post_command(pexer.commands.EXIT)
            return True
        return False


class App(pexer.TApplication):
    def on_start(self):
        self.add_window(pexer.TWindow(
            position=pexer.TPoint(8, 4), size=pexer.TSize(50, 12),
            title="Click inside me to quit",
            content=QuitWidget()
        ))
        self.add_window(pexer.TWindow(
            position=pexer.TPoint(10, 6), size=pexer.TSize(60, 8),
            title="Filled window", content=pexer.TFill(
                fill_character="X", fill_attributes="twindow.background"
            )
        ))


if __name__ == "__main__":
    App().run()
