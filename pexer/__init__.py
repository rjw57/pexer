"""
The Pexer library provides a text-based user interface akin to the `Turbo Vision`_. It is based on
the `Jexer`_ project by Autumn Lamonte.

.. _Turbo Vision: https://en.wikipedia.org/wiki/Turbo_Vision
.. _Jexer: https://gitlab.com/klamonte/jexer

"""
from .size import *  # noqa: F403,F401
from .tapplication import *  # noqa: F403,F401
from .tbox import *  # noqa: F403,F401
from .tcommand import *  # noqa: F403,F401
from .tdesktop import *  # noqa: F403,F401
from .tfill import *  # noqa: F403,F401
from .tkeypress import *  # noqa: F403,F401
from .tpoint import *  # noqa: F403,F401
from .twidget import *  # noqa: F403,F401
from .twindow import *  # noqa: F403,F401
