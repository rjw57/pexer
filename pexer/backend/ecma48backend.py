from .ecma48terminal import ECMA48Terminal
from .genericbackend import GenericBackend
from .ttysessioninfo import TTYSessionInfo

__all__ = ['ECMA48Backend']


class ECMA48Backend(GenericBackend):
    """
    This class uses an xterm/ANSI X3.64/ECMA-48 type terminal to provide a
    screen, keyboard, and mouse to TApplication.

    """
    def __init__(self) -> None:
        session_info = TTYSessionInfo()
        screen_and_terminal = ECMA48Terminal(self)
        super().__init__(
            session_info=session_info, screen=screen_and_terminal,
            terminal=screen_and_terminal
        )
