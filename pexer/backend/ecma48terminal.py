import asyncio
import codecs
import datetime
import enum
import locale
import signal
import subprocess
import sys
import traceback
from typing import List, Optional, TextIO

from aioconsole.stream import create_standard_streams  # type: ignore

from ..bits import CellAttributes, Color, CellFlags, Cell, CellWidth, graphicschars
from ..event import TKeypressEvent, TResizeEvent, TMouseEvent, TInputEvent
from ..tkeypress import TKeypress

from .genericbackend import GenericBackend
from .logicalscreen import LogicalScreen
from .terminalreader import TerminalReader
from .ttysessioninfo import TTYSessionInfo

__all__ = ['ECMA48Terminal']


class ECMA48Terminal(TerminalReader, LogicalScreen):
    """
    This class reads keystrokes and mouse events and emits output to ANSI X3.64 / ECMA-48 type
    terminals e.g. xterm, linux, vt100, ansi.sys, etc.

    """
    class ParseState(enum.Enum):
        "States in the input parser."
        GROUND = enum.auto()
        ESCAPE = enum.auto()
        ESCAPE_INTERMEDIATE = enum.auto()
        CSI_ENTRY = enum.auto()
        CSI_PARAM = enum.auto()
        MOUSE = enum.auto()
        MOUSE_SGR = enum.auto()

    def __init__(
            self, backend: GenericBackend, *,
            input: Optional[TextIO] = None, output: Optional[TextIO] = None,
            set_raw_mode: bool = True) -> None:
        """
        backend - the backend that will read from this terminal
        input - an file-like object connected to the remote user, or None for
            sys.stdio. If sys.stdio is used, then on non-Windows systems it will
            be put in raw mode; close() will (blindly!) put System.in in
            cooked mode.  input should return strings with UTF-8 cencoding
            encoding.
        output - a file-like object connected to the remote user, or None
            for sys.stdout.

        """
        super().__init__()

        self.input: TextIO = input if input is not None else sys.stdin
        self.output: TextIO = output if output is not None else sys.stdout
        self.input_stream, self.output_stream = None, None
        self.params: List[str] = []
        self.state: ECMA48Terminal.ParseState = self.ParseState.GROUND
        self.escape_time: datetime.datetime = datetime.datetime.utcnow()

        self.do_rgb_color = True

        self.backend = backend
        self.session_info = TTYSessionInfo()

        self._reset_parser()

        # True if mouse {1, 2, 3} was down. Used to report mouseX on the release event.
        self.mouse1 = self.mouse2 = self.mouse3 = False

        # Cache the cursor visibility value so we only emit the sequence when we need to.
        self.cursor_on = True

        # If true, then we changed stdin and need to change it back.
        self.set_raw_mode = set_raw_mode

        # Timer fired when an escape key is received which is used to treat it as a control
        # character after a delay.
        self._esc_timer: Optional[asyncio.TimerHandle] = None

        self.output_queue: 'Optional[asyncio.Queue[str]]' = None
        self._event_queue: 'Optional[asyncio.Queue[TInputEvent]]' = None

        self.window_resize: Optional[TResizeEvent] = None

        self.was_opened = False

    @property
    def event_queue(self) -> 'Optional[asyncio.Queue[TInputEvent]]':
        return self._event_queue

    def open(self, event_queue: asyncio.Queue) -> None:
        if self.set_raw_mode:
            self._stty_raw()

        self.output_queue = asyncio.Queue()
        self._event_queue = event_queue

        # Query the screen size
        self.session_info.query_window_size()
        self.set_dimensions(
            self.session_info.window_width, self.session_info.window_height
        )

        # Cache the most recent window resize event
        self.window_resize = TResizeEvent(
            self.backend, TResizeEvent.Type.SCREEN,
            self.session_info.window_width, self.session_info.window_height
        )

        # Spin up the output task.
        self.writer_task = asyncio.create_task(self._run_writer())

        # Spin up the input reader asyncio task
        self.reader_task = asyncio.create_task(self._run_reader())

        # Spin up the window size watcher asyncio task
        self.resize_task = asyncio.create_task(self._run_resize())

        out_strs = [
            # Request device attributes
            '\033[c',

            # Request xterm report window/cell dimensions in pixels
            self._xterm_report_pixel_dimensions(),

            # Enable mouse reporting and metaSendsEscape
            self._mouse(True),
            self._xterm_meta_sends_escape(True),

            # Set the title
            self._get_set_title_string(self._title),

            # Clear the screen
            self._clear_all(),
        ]
        self.output_queue.put_nowait(''.join(out_strs))

        self.was_opened = True

    def close(self) -> None:
        """
        Restore terminal to normal state.

        """
        if not self.was_opened:
            return

        self.was_opened = False

        self.writer_task.cancel()
        self.reader_task.cancel()
        self.resize_task.cancel()

        self.output.write(self._mouse(False))
        self.output.write(self._cursor(True))
        self.output.write(self._default_color())
        self.output.write('\033[>4m')
        self.output.flush()

        self.output_queue = None
        self._event_queue = None

        if self.set_raw_mode:
            self._stty_cooked()
            self.set_raw_mode = False

    def flush_physical(self) -> None:
        """
        Push the logical screen to the physical device.

        """
        assert self.output_queue is not None

        # List of fragments to write to output device.
        out_strs: List[str] = []

        if (self.cursor_visible and self.cursor_x >= 0 and self.cursor_y >= 0
                and self.cursor_x <= self.width - 1 and self.cursor_y <= self.height - 1):
            self._flush_string(out_strs)
            out_strs.append(self._cursor(True))
            out_strs.append(self._goto_x_y(self.cursor_x, self.cursor_y))
        else:
            out_strs.append(self._cursor(False))
            self._flush_string(out_strs)

        self.output_queue.put_nowait(''.join(out_strs))

    def flush(self) -> None:
        self.output.flush()

    def set_title(self, title: str) -> None:
        super().set_title(title)
        if self.output_queue is not None:
            self.output_queue.put_nowait(self._get_set_title_string(title))
            self.flush_physical()

    async def _ensure_streams(self) -> None:
        if self.input_stream is None or self.output_stream is None:
            self.input_stream, self.output_stream, _ = await create_standard_streams(
                self.input, self.output, sys.stderr, loop=asyncio.get_event_loop()
            )

    async def _run_writer(self) -> None:
        await self._ensure_streams()
        try:
            while True:
                if self.output_queue is None:
                    break
                out_str = await self.output_queue.get()
                if self.output_stream is None:
                    break
                self.output_stream.write(out_str)
                await self.output_stream.drain()
                self.output_queue.task_done()
        except asyncio.CancelledError:
            pass
        except Exception:
            self.close()
            traceback.print_exc()
            sys.exit(1)

    async def _run_reader(self) -> None:
        await self._ensure_streams()
        try:
            decoder = codecs.getincrementaldecoder(locale.getpreferredencoding())(errors='ignore')
            while True:
                if self.input_stream is None:
                    break
                for ch in decoder.decode(await self.input_stream.read(1)):
                    self._process_char(ord(ch))
        except asyncio.CancelledError:
            pass
        except Exception:
            self.close()
            traceback.print_exc()
            sys.exit(1)

    async def _run_resize(self) -> None:
        """
        Poll window size regularly on in response to SIGWINCH signal.

        """
        loop = asyncio.get_running_loop()
        event = asyncio.Event()
        try:
            loop.add_signal_handler(signal.SIGWINCH, event.set)
            while True:
                try:
                    await asyncio.wait_for(event.wait(), timeout=5)
                    event.clear()
                except asyncio.TimeoutError:
                    pass

                old_size = (self.session_info.window_width, self.session_info.window_height)
                self.session_info.query_window_size()
                new_size = (self.session_info.window_width, self.session_info.window_height)
                if old_size == new_size:
                    continue

                self.window_resize = TResizeEvent(
                    self.backend, TResizeEvent.Type.SCREEN,
                    self.session_info.window_width, self.session_info.window_height
                )
                if self._event_queue is None:
                    break
                self._event_queue.put_nowait(self.window_resize)
        except asyncio.CancelledError:
            pass
        except Exception:
            self.close()
            traceback.print_exc()
            sys.exit(1)
        finally:
            loop.remove_signal_handler(signal.SIGWINCH)

    def _cancel_esc(self) -> None:
        self._esc_timer = None
        if self.state != self.ParseState.ESCAPE or self._event_queue is None:
            return
        self._event_queue.put_nowait(self._control_char(0x1b, False))
        self._reset_parser()

    def _process_char(self, ch: int) -> None:
        """
        Parses the next character of input to see if an InputEvent is fully here.

        """
        # HACK: uncomment for debugging
        # print(f'{self._goto_x_y(0,0)}{ch} - {chr(ch) if ch >= 0x20 else ch} - {self.state}\r')

        assert self._event_queue is not None

        # ESCDELAY type timeout
        now = datetime.datetime.utcnow()
        if self.state == self.ParseState.ESCAPE:
            esc_delay = now - self.escape_time
            if esc_delay.total_seconds() > 0.05:
                # After 0.05 seconds, assume a true escape char
                self._cancel_esc()

        # TKeypress fields
        ctrl, alt, shift = False, False, False

        if self.state == self.ParseState.GROUND:
            if ch == 0x1b:
                self.state = self.ParseState.ESCAPE
                self.escape_time = now
                if self._esc_timer is not None:
                    self._esc_timer.cancel()
                self._esc_timer = asyncio.get_running_loop().call_later(0.05, self._cancel_esc)
                return
            elif ch <= 0x1f:
                # Control character
                self._event_queue.put_nowait(self._control_char(ch, False))
                self._reset_parser()
                return
            elif ch >= 0x20:
                # Normal character
                self._event_queue.put_nowait(TKeypressEvent(
                    self.backend, False, 0, ch, False, False, False))
                return
        elif self.state == self.ParseState.ESCAPE:
            if ch <= 0x1f:
                # ALT-Control character
                self._event_queue.put_nowait(self._control_char(ch, True))
                self._reset_parser()
                return
            if ch == ord('O'):
                # This will be one of the function keys
                self.state = self.ParseState.ESCAPE_INTERMEDIATE
                return
            if ch == ord('['):
                # '[' goes to CSI_ENTRY
                self.state = self.ParseState.CSI_ENTRY
                return
            # Everything else is assumed to be Alt-keystroke
            if ch >= ord('A') and ch <= ord('Z'):
                shift = True
            alt = True
            self._event_queue.put_nowait(TKeypressEvent(
                self.backend, False, 0, ch, alt, ctrl, shift))
            self._reset_parser()
            return
        elif self.state == self.ParseState.ESCAPE_INTERMEDIATE:
            if ch >= ord('P') and ch <= ord('S'):
                # Function key
                if ch == ord('P'):
                    self._event_queue.put_nowait(TKeypressEvent(
                        self.backend, True, TKeypress.F1, ord(' '), False, False, False))
                elif ch == ord('Q'):
                    self._event_queue.put_nowait(TKeypressEvent(
                        self.backend, True, TKeypress.F2, ord(' '), False, False, False))
                elif ch == ord('R'):
                    self._event_queue.put_nowait(TKeypressEvent(
                        self.backend, True, TKeypress.F3, ord(' '), False, False, False))
                elif ch == ord('S'):
                    self._event_queue.put_nowait(TKeypressEvent(
                        self.backend, True, TKeypress.F4, ord(' '), False, False, False))
                self._reset_parser()
                return

            # Unknown keystroke, ignore
            self._reset_parser()
            return
        elif self.state == self.ParseState.CSI_ENTRY:
            # Numbers - parameter values
            if ch >= ord('0') and ch <= ord('9'):
                self.params[-1] = self.params[-1] + chr(ch)
                self.state = self.ParseState.CSI_PARAM
                return

            # Parameter separator
            if ch == ord(';'):
                self.params.append('')
                return

            if ch >= 0x30 and ch <= 0x7E:
                if ch == ord('A'):
                    # Up
                    self._event_queue.put_nowait(TKeypressEvent(
                        self.backend, True, TKeypress.UP, ord(' '), alt, ctrl, shift))
                    self._reset_parser()
                    return
                if ch == ord('B'):
                    # Down
                    self._event_queue.put_nowait(TKeypressEvent(
                        self.backend, True, TKeypress.DOWN, ord(' '), alt, ctrl, shift))
                    self._reset_parser()
                    return
                if ch == ord('C'):
                    # Right
                    self._event_queue.put_nowait(TKeypressEvent(
                        self.backend, True, TKeypress.RIGHT, ord(' '), alt, ctrl, shift))
                    self._reset_parser()
                    return
                if ch == ord('D'):
                    # Left
                    self._event_queue.put_nowait(TKeypressEvent(
                        self.backend, True, TKeypress.LEFT, ord(' '), alt, ctrl, shift))
                    self._reset_parser()
                    return
                if ch == ord('H'):
                    # Home
                    self._event_queue.put_nowait(TKeypressEvent(
                        self.backend, True, TKeypress.HOME, ord(' '), False, False, False))
                    self._reset_parser()
                    return
                if ch == ord('F'):
                    # End
                    self._event_queue.put_nowait(TKeypressEvent(
                        self.backend, True, TKeypress.END, ord(' '), False, False, False))
                    self._reset_parser()
                    return
                if ch == ord('Z'):
                    # CBT - Cursor backward X tab stops (default 1)
                    self._event_queue.put_nowait(TKeypressEvent(
                        self.backend, True, TKeypress.BTAB, ord(' '), False, False, False))
                    self._reset_parser()
                    return
                if ch == ord('M'):
                    # Mouse position
                    self.state = self.ParseState.MOUSE
                    return
                if ch == ord('<'):
                    # Mouse position, SGR (1006) coordinates
                    self.state = self.ParseState.MOUSE_SGR
                    return
                if ch == ord('?'):
                    # DEC private mode flag
                    self.dec_private_mode_flag = True
                    return

            # Unknown keystroke, ignore
            self._reset_parser()
            return
        elif self.state == self.ParseState.MOUSE_SGR:
            # Numbers - parameter values
            if ch >= ord('0') and ch <= ord('9'):
                self.params[-1] = self.params[-1] + chr(ch)
                return
            # Parameter separator
            if ch == ord(';'):
                self.params.append('')
                return
            if ch == ord('M'):
                # Generate a mouse press event
                event = self._parse_mouse_sgr(False)
                if event is not None:
                    self._event_queue.put_nowait(event)
                self._reset_parser()
                return
            if ch == ord('m'):
                # Generate a mouse release event
                event = self._parse_mouse_sgr(True)
                if event is not None:
                    self._event_queue.put_nowait(event)
                self._reset_parser()
                return

            # Unknown keystroke, ignore
            self._reset_parser()
            return
        elif self.state == self.ParseState.CSI_PARAM:
            # Numbers - parameter values
            if ch >= ord('0') and ch <= ord('9'):
                self.params[-1] = self.params[-1] + chr(ch)
                self.state = self.ParseState.CSI_PARAM
                return

            # Parameter separator
            if ch == ord(';'):
                self.params.append('')
                return

            if ch == ord('~'):
                fn_event = self._csi_fn_key()
                if fn_event is not None:
                    self._event_queue.put_nowait(fn_event)
                self._reset_parser()
                return

            if ch >= 0x30 and ch <= 0x7E:
                if ch == ord('A'):
                    # Up
                    if len(self.params) > 1:
                        shift = self._csi_is_shift(self.params[1])
                        alt = self._csi_is_alt(self.params[1])
                        ctrl = self._csi_is_ctrl(self.params[1])
                    self._event_queue.put_nowait(TKeypressEvent(
                        self.backend, True, TKeypress.UP, ord(' '), alt, ctrl, shift))
                    self._reset_parser()
                    return
                if ch == ord('B'):
                    # Down
                    if len(self.params) > 1:
                        shift = self._csi_is_shift(self.params[1])
                        alt = self._csi_is_alt(self.params[1])
                        ctrl = self._csi_is_ctrl(self.params[1])
                    self._event_queue.put_nowait(TKeypressEvent(
                        self.backend, True, TKeypress.DOWN, ord(' '), alt, ctrl, shift))
                    self._reset_parser()
                    return
                if ch == ord('C'):
                    # Right
                    if len(self.params) > 1:
                        shift = self._csi_is_shift(self.params[1])
                        alt = self._csi_is_alt(self.params[1])
                        ctrl = self._csi_is_ctrl(self.params[1])
                    self._event_queue.put_nowait(TKeypressEvent(
                        self.backend, True, TKeypress.RIGHT, ord(' '), alt, ctrl, shift))
                    self._reset_parser()
                    return
                if ch == ord('D'):
                    # Left
                    if len(self.params) > 1:
                        shift = self._csi_is_shift(self.params[1])
                        alt = self._csi_is_alt(self.params[1])
                        ctrl = self._csi_is_ctrl(self.params[1])
                    self._event_queue.put_nowait(TKeypressEvent(
                        self.backend, True, TKeypress.LEFT, ord(' '), alt, ctrl, shift))
                    self._reset_parser()
                    return
                if ch == ord('H'):
                    # Home
                    if len(self.params) > 1:
                        shift = self._csi_is_shift(self.params[1])
                        alt = self._csi_is_alt(self.params[1])
                        ctrl = self._csi_is_ctrl(self.params[1])
                    self._event_queue.put_nowait(TKeypressEvent(
                        self.backend, True, TKeypress.HOME, ord(' '), alt, ctrl, shift))
                    self._reset_parser()
                    return
                if ch == ord('F'):
                    # End
                    if len(self.params) > 1:
                        shift = self._csi_is_shift(self.params[1])
                        alt = self._csi_is_alt(self.params[1])
                        ctrl = self._csi_is_ctrl(self.params[1])
                    self._event_queue.put_nowait(TKeypressEvent(
                        self.backend, True, TKeypress.END, ord(' '), alt, ctrl, shift))
                    self._reset_parser()
                    return
                if ch == ord('s'):
                    # Device Attributes
                    # TODO
                    self._reset_parser()
                    return
                if ch == ord('t'):
                    # windowOps
                    # TODO
                    self._reset_parser()
                    return

            # Unknown keystroke, ignore
            self._reset_parser()
            return
        elif self.state == self.ParseState.MOUSE:
            self.params[0] = self.params[-1] + chr(ch)
            if len(self.params[0]) == 3:
                # We have enough to generate a mouse event
                event = self._parse_mouse()
                if event is not None:
                    self._event_queue.put_nowait(event)
                self._reset_parser()
            return

    def _parse_mouse(self) -> TMouseEvent:
        # TODO
        raise NotImplementedError()

    def _parse_mouse_sgr(self, release: bool) -> Optional[TMouseEvent]:
        """
        Produce mouse events based on "Any event tracking" and SGR
        coordinates.  See
        http://invisible-island.net/xterm/ctlseqs/ctlseqs.html#Mouse%20Tracking

        release - if true, this was a release ('m')

        Returns a MOUSE_MOTION, MOUSE_UP, or MOUSE_DOWN event

        """
        assert self.window_resize is not None

        # SGR extended coordinates - mode 1006
        if len(self.params) < 3:
            # Invalid position, bail out.
            return None

        buttons = int(self.params[0])
        x = int(self.params[1]) - 1
        y = int(self.params[2]) - 1

        # Clamp X and Y to the physical screen coordinates.
        if x >= self.window_resize.width:
            x = self.window_resize.width - 1
        if y >= self.window_resize.height:
            y = self.window_resize.height - 1

        event_type = TMouseEvent.Type.MOUSE_DOWN
        event_mouse1, event_mouse2, event_mouse3 = False, False, False
        event_mouse_wheel_up, event_mouse_wheel_down = False, False
        event_alt, event_ctrl, event_shift = False, False, False

        if release:
            event_type = TMouseEvent.Type.MOUSE_UP

        event_code = buttons & 0xE3
        if event_code == 0:
            event_mouse1 = True
        elif event_code == 1:
            event_mouse2 = True
        elif event_code == 2:
            event_mouse3 = True
        elif event_code == 35:
            # Motion only, no buttons down
            event_type = TMouseEvent.Type.MOUSE_MOTION
        elif event_code == 32:
            # Dragging with mouse 1 down
            event_mouse1 = True
            event_type = TMouseEvent.Type.MOUSE_MOTION
        elif event_code == 33:
            # Dragging with mouse 2 down
            event_mouse2 = True
            event_type = TMouseEvent.Type.MOUSE_MOTION
        elif event_code == 34:
            # Dragging with mouse 3 down
            event_mouse3 = True
            event_type = TMouseEvent.Type.MOUSE_MOTION
        elif event_code == 96:
            # Dragging with mouse 2 down after wheelUp
            event_mouse2 = True
            event_type = TMouseEvent.Type.MOUSE_MOTION
        elif event_code == 97:
            # Dragging with mouse 2 down after wheelDown
            event_mouse2 = True
            event_type = TMouseEvent.Type.MOUSE_MOTION
        elif event_code == 64:
            event_mouse_wheel_up = True
        elif event_code == 65:
            event_mouse_wheel_down = True

        if buttons & 0x04 != 0:
            event_shift = True
        if buttons & 0x08 != 0:
            event_alt = True
        if buttons & 0x10 != 0:
            event_ctrl = True

        return TMouseEvent(
            self.backend, event_type, x, y, x, y, event_mouse1, event_mouse2, event_mouse3,
            event_mouse_wheel_up, event_mouse_wheel_down, event_alt, event_ctrl, event_shift
        )

    def _csi_is_shift(self, x: str) -> bool:
        """
        Returns true if the CSI parameter for a keyboard command means that
        shift was down.

        """
        return x in '2468'

    def _csi_is_alt(self, x: str) -> bool:
        """
        Returns true if the CSI parameter for a keyboard command means that
        alt was down.

        """
        return x in '3478'

    def _csi_is_ctrl(self, x: str) -> bool:
        """
        Returns true if the CSI parameter for a keyboard command means that
        ctrl was down.

        """
        return x in '5678'

    def _csi_fn_key(self) -> Optional[TKeypressEvent]:
        """
        Produce special key from CSI Pn ; Pm ; ... ~

        Returns one KEYPRESS event representing a special key

        """
        key = 0
        if len(self.params) > 0:
            key = int(self.params[0])
        alt, ctrl, shift = False, False, False

        other_key = 0
        if len(self.params) > 1:
            shift = self._csi_is_shift(self.params[1])
            alt = self._csi_is_alt(self.params[1])
            ctrl = self._csi_is_ctrl(self.params[1])
        if len(self.params) > 2:
            other_key = int(self.params[2])

        if key == 1:
            return TKeypressEvent(self.backend, True, TKeypress.HOME, ord(' '), alt, ctrl, shift)
        elif key == 2:
            return TKeypressEvent(self.backend, True, TKeypress.INS, ord(' '), alt, ctrl, shift)
        elif key == 3:
            return TKeypressEvent(self.backend, True, TKeypress.DEL, ord(' '), alt, ctrl, shift)
        elif key == 4:
            return TKeypressEvent(self.backend, True, TKeypress.END, ord(' '), alt, ctrl, shift)
        elif key == 5:
            return TKeypressEvent(self.backend, True, TKeypress.PGUP, ord(' '), alt, ctrl, shift)
        elif key == 6:
            return TKeypressEvent(self.backend, True, TKeypress.PGDN, ord(' '), alt, ctrl, shift)
        elif key == 15:
            return TKeypressEvent(self.backend, True, TKeypress.F5, ord(' '), alt, ctrl, shift)
        elif key == 17:
            return TKeypressEvent(self.backend, True, TKeypress.F6, ord(' '), alt, ctrl, shift)
        elif key == 18:
            return TKeypressEvent(self.backend, True, TKeypress.F7, ord(' '), alt, ctrl, shift)
        elif key == 19:
            return TKeypressEvent(self.backend, True, TKeypress.F8, ord(' '), alt, ctrl, shift)
        elif key == 20:
            return TKeypressEvent(self.backend, True, TKeypress.F9, ord(' '), alt, ctrl, shift)
        elif key == 21:
            return TKeypressEvent(self.backend, True, TKeypress.F10, ord(' '), alt, ctrl, shift)
        elif key == 23:
            return TKeypressEvent(self.backend, True, TKeypress.F11, ord(' '), alt, ctrl, shift)
        elif key == 24:
            return TKeypressEvent(self.backend, True, TKeypress.F12, ord(' '), alt, ctrl, shift)
        elif key == 27:
            # modifyOtherKeys sequence
            if other_key == 8:
                return TKeypressEvent(
                    self.backend, True, TKeypress.BACKSPACE, ord(' '), alt, ctrl, shift)
            elif other_key == 9:
                return TKeypressEvent(
                    self.backend, True, TKeypress.TAB, ord(' '), alt, ctrl, shift)
            elif other_key == 13:
                return TKeypressEvent(
                    self.backend, True, TKeypress.ENTER, ord(' '), alt, ctrl, shift)
            elif other_key == 27:
                return TKeypressEvent(
                    self.backend, True, TKeypress.ESC, ord(' '), alt, ctrl, shift)
            elif other_key >= ord('a') and other_key <= ord('z') and ctrl:
                # turn Ctrl-lowercase into Ctrl-uppercase
                return TKeypressEvent(
                    self.backend, False, 0, other_key - 32, alt, ctrl, shift)
            else:
                return TKeypressEvent(
                    self.backend, False, 0, other_key, alt, ctrl, shift)

        return None

    def _control_char(self, ch: int, alt: bool) -> TKeypressEvent:
        """
        Produce a control character or one of the special ones (ENTER, TAB,
        etc.).

        ch - Unicode code point
        alt - if true, set alt on the TKeypress

        Returns one TKeypress event, either a control character (e.g. isKey == false, ch == 'A',
        ctrl == true), or a special key (e.g. isKey == true, fnKey == ESC)

        """
        if ch == 0x0D or ch == 0x0A:
            # Carriage return or linefeed --> ENTER
            return TKeypressEvent(self.backend, True, TKeypress.ENTER, ord(' '), alt, False, False)
        elif ch == 0x1B:
            # ESC
            return TKeypressEvent(self.backend, True, TKeypress.ESC, ord(' '), alt, False, False)
        elif ch == 0x09:
            # Tab
            return TKeypressEvent(self.backend, True, TKeypress.TAB, ord(' '), alt, False, False)

        # Make all other control characters come back as the alphabetic character with the ctrl
        # field set.  So SOH would be 'A' + ctrl.
        return TKeypressEvent(self.backend, False, 0, ch + 0x40, alt, True, False)

    def _reset_parser(self) -> None:
        # Current parsing state
        self.state = self.ParseState.GROUND

        # Parameters being collected.  E.g. if the string is \033[1;3m, then params[0] will be 1
        # and params[1] will be 3.
        self.params = ['']

        # If true, '?' was seen in terminal resoinse.
        self.dec_private_mode_flag = False

    def _cursor(self, on: bool) -> str:
        """
        Create a SGR parameter sequence for enabling the visible cursor.

        on - if true, turn on cursor

        Returns the string to emit to an ANSI / ECMA-style terminal

        """
        if on and not self.cursor_on:
            self.cursor_on = True
            return '\033[?25h'
        if not on and self.cursor_on:
            self.cursor_on = False
            return '\033[?25l'
        return ''

    def _clear_remaining_line(self) -> str:
        """
        Clear the line from the cursor (inclusive) to the end of the screen.
        Because some terminals use back-color-erase, set the color to
        white-on-black beforehand.

        Returns the string to emit to an ANSI / ECMA-style terminal

        """
        return '\033[0;37;40m\033[K'

    def _goto_x_y(self, x: int, y: int) -> str:
        """
        Move the cursor to (x, y).

        x - column coordinate.  0 is the left-most column.
        y - row coordinate.  0 is the top-most row.

        Returns the string to emit to an ANSI / ECMA-style terminal

        """
        return f'\033[{y+1};{x+1}H'

    def _normal(self, header: bool = True) -> str:
        """
        Create a SGR parameter sequence to reset to VT100 defaults.

        header - if true, make the full header, otherwise just emit the bare parameter e.g. "0;"

        Returns the string to emit to an ANSI / ECMA-style terminal, e.g. "\033[0m"

        """
        if header:
            return ''.join([
                '\033[0;37;40m',
                self._rgb_color_bold_fore_back(False, Color.WHITE, Color.BLACK)
            ])
        return '0;37;40'

    def _default_color(self) -> str:
        """
        Create a SGR parameter sequence to reset to ECMA-48 default foreground/background.

        Returns the string to emit to an ANSI / ECMA-style terminal, e.g. "\033[0m"

        """
        # VT100 normal.
        # Normal (neither bold nor faint).
        # Not italicized.
        # Not underlined.
        # Steady (not blinking).
        # Positive (not inverse).
        # Visible (not hidden).
        # Not crossed-out.
        # Default foreground color.
        # Default background color.
        return '\033[0;22;23;24;25;27;28;29;39;49m'

    def _system_color_rgb(self, color: Color) -> str:
        """
        Create a T.416 RGB parameter sequence for a custom system color.

        color - one of the MYBLACK, MYBOLD_BLUE, etc. colors
        Returns the color portion of the string to emit to an ANSI /
        ECMA-style terminal

        """
        return f'{color.red};{color.green};{color.blue}'

    def _rgb_color_bold_color_foreground(self, bold: bool, color: Color, foreground: bool) -> str:
        """
        Create a T.416 RGB parameter sequence for a single color change.

        bold - if true, set bold
        color - one of the Color.WHITE, Color.BLUE, etc. constants
        foreground - if true, this is a foreground color
        Returns the string to emit to an xterm terminal with RGB support,
        e.g. "\033[38;2;RR;GG;BBm"

        """
        if not self.do_rgb_color:
            return ''
        out_strs = ['\033[']
        if foreground:
            out_strs.append('38;2;')
        else:
            out_strs.append('48;2;')
        out_strs.append(self._system_color_rgb(color))
        out_strs.append('m')
        return ''.join(out_strs)

    def _rgb_color_bold_fore_back(self, bold: bool, fore_color: Color, back_color: Color) -> str:
        """
        Create a T.416 RGB parameter sequence for both foreground and
        background color change.

        bold - if true, set bold
        fore_color - one of the Color.WHITE, Color.BLUE, etc. constants
        back_color - one of the Color.WHITE, Color.BLUE, etc. constants
        Return the string to emit to an xterm terminal with RGB support,
        e.g. "\033[38;2;RR;GG;BB;48;2;RR;GG;BBm"

        """
        if not self.do_rgb_color:
            return ''

        return (
            self._rgb_color_bold_color_foreground(bold, fore_color, True) +
            self._rgb_color_bold_color_foreground(False, back_color, False)
        )

    def _color_bold_color_foreground(self, bold: bool, color: Color, foreground: bool) -> str:
        """
        Create a SGR parameter sequence for a single color change.

        bold - if true, set bold
        color - one of the Color.WHITE, Color.BLUE, etc. constants
        foreground - if true, this is a foreground color
        Returns the string to emit to an ANSI / ECMA-style terminal,
        e.g. "\033[42m"

        """
        return (
            self._color_color_foreground_header(color, foreground, True) +
            self._rgb_color_bold_color_foreground(bold, color, foreground)
        )

    def _color_color_foreground_header(self, color: Color, foreground: bool, header: bool) -> str:
        """
        Create a SGR parameter sequence for a single color change.

        color - one of the Color.WHITE, Color.BLUE, etc. constants
        foreground - if true, this is a foreground color
        header - if true, make the full header, otherwise just emit the
        color parameter e.g. "42;"
        Returns the string to emit to an ANSI / ECMA-style terminal,
        e.g. "\033[42m"

        """
        ecma_color = _ECMA48_COLORS.get(color, 7)

        # Convert Color.* values to SGR numerics
        if foreground:
            ecma_color += 30
        else:
            ecma_color += 40

        if header:
            return f'\033[{ecma_color}m'
        else:
            return f'{ecma_color};'

    def _color_bold_fore_back(self, bold: bool, fore_color: Color, back_color: Color) -> str:
        """
        Create a SGR parameter sequence for both foreground and background
        color change.

        bold - if true, set bold
        fore_color - one of the Color.WHITE, Color.BLUE, etc. constants
        back_color - one of the Color.WHITE, Color.BLUE, etc. constants
        Returns the string to emit to an ANSI / ECMA-style terminal,
        e.g. "\033[31;42m"

        """
        return ''.join([
            self._color_fore_back_header(fore_color, back_color, True),
            self._rgb_color_bold_fore_back(bold, fore_color, back_color)
        ])

    def _color_fore_back_header(self, fore_color: Color, back_color: Color, header: bool) -> str:
        """
        Create a SGR parameter sequence for both foreground and
        background color change.

        fore_color - one of the Color.WHITE, Color.BLUE, etc. constants
        back_color - one of the Color.WHITE, Color.BLUE, etc. constants
        header - if true, make the full header, otherwise just emit the
        color parameter e.g. "31;42;"
        Returns the string to emit to an ANSI / ECMA-style terminal,
        e.g. "\033[31;42m"

        """
        ecma_fore_color = _ECMA48_COLORS.get(fore_color, 7)
        ecma_back_color = _ECMA48_COLORS.get(back_color, 0)

        # Convert Color.* values to SGR numerics
        ecma_back_color += 40
        ecma_fore_color += 30

        if header:
            return f'\033[{ecma_fore_color};{ecma_back_color}m'
        else:
            return f'{ecma_fore_color};{ecma_back_color};'

    def _color(
            self, fore_color: Color, back_color: Color, bold: bool, reverse: bool, blink: bool,
            underline: bool) -> str:
        """
        Create a SGR parameter sequence for foreground, background, and
        several attributes.  This sequence first resets all attributes to
        default, then sets attributes as per the parameters.

        fore_color - one of the Color.WHITE, Color.BLUE, etc. constants
        back_color - one of the Color.WHITE, Color.BLUE, etc. constants
        bold - if true, set bold
        reverse - if true, set reverse
        blink - if true, set blink
        underline - if true, set underline
        Returns the string to emit to an ANSI / ECMA-style terminal,
        e.g. "\033[0;1;31;42m"

        """
        ecma_fore_color = _ECMA48_COLORS.get(fore_color, 7)
        ecma_back_color = _ECMA48_COLORS.get(back_color, 0)

        # Convert Color.* values to SGR numerics
        ecma_back_color += 40
        ecma_fore_color += 30

        out_strs = []
        if bold and reverse and blink and not underline:
            out_strs.append('\033[0;1;7;5;')
        elif bold and reverse and not blink and not underline:
            out_strs.append('\033[0;1;7;')
        elif not bold and reverse and blink and not underline:
            out_strs.append('\033[0;7;5;')
        elif bold and not reverse and blink and not underline:
            out_strs.append('\033[0;1;5;')
        elif bold and not reverse and not blink and not underline:
            out_strs.append('\033[0;1;')
        elif not bold and reverse and not blink and not underline:
            out_strs.append('\033[0;7;')
        elif not bold and not reverse and blink and not underline:
            out_strs.append('\033[0;5;')
        elif bold and reverse and blink and underline:
            out_strs.append('\033[0;1;7;5;4;')
        elif bold and reverse and not blink and underline:
            out_strs.append('\033[0;1;7;4;')
        elif not bold and reverse and blink and underline:
            out_strs.append('\033[0;7;5;4;')
        elif bold and not reverse and blink and underline:
            out_strs.append('\033[0;1;5;4;')
        elif bold and not reverse and not blink and underline:
            out_strs.append('\033[0;1;4;')
        elif not bold and reverse and not blink and underline:
            out_strs.append('\033[0;7;4;')
        elif not bold and not reverse and blink and underline:
            out_strs.append('\033[0;5;4;')
        elif not bold and not reverse and not blink and underline:
            out_strs.append('\033[0;4;')
        else:
            assert (not bold and not reverse and not blink and not underline)
            out_strs.append('\033[0;')

        out_strs.append(f'{ecma_fore_color};{ecma_back_color}m')
        out_strs.append(self._rgb_color_bold_fore_back(
            bold, fore_color, back_color
        ))
        return ''.join(out_strs)

    def _xterm_report_pixel_dimensions(self) -> str:
        """
        Request (u)xterm to report the current window and cell size dimensions
        in pixels.

        Returns the string to emit to xterm
        """
        return '\033[16t\033[14t'

    def _xterm_meta_sends_escape(self, on: bool) -> str:
        """
        Tell (u)xterm that we want alt- keystrokes to send escape + character
        rather than set the 8th bit.  Anyone who wants UTF8 should want this
        enabled.

        on - if true, enable metaSendsEscape

        Returns the string to emit to xterm

        """
        if on:
            return '\033[?1036h\033[?1034l'
        return '\033[?1036l'

    def _get_set_title_string(self, title: str) -> str:
        """
        Create an xterm OSC sequence to change the window title.

        title - the new title
        Returns the string to emit to xterm

        """
        return f'\033]2;{title}\007'

    def _mouse(self, on: bool) -> str:
        """
        Tell (u)xterm that we want to receive mouse events based on "Any event
        tracking", UTF-8 coordinates, and then SGR coordinates.  Ideally we
        will end up with SGR coordinates with UTF-8 coordinates as a fallback.
        See
        http://invisible-island.net/xterm/ctlseqs/ctlseqs.html#Mouse%20Tracking

        Note that this also sets the alternate/primary screen buffer.

        Finally, also emit a Privacy Message sequence that Pexer recognizes to
        mean "hide the mouse pointer."  We have to use our own sequence to do
        this because there is no standard in xterm for unilaterally hiding the
        pointer all the time (regardless of typing).

        on - If true, enable mouse report and use the alternate screen
            buffer.  If false disable mouse reporting and use the primary screen
            buffer.

        Returns the string to emit to xterm

        """
        if on:
            return '\033[?1002;1003;1005;1006h\033[?1049h\033^hideMousePointer\033\\'
        return '\033[?1002;1003;1006;1005l\033[?1049l\033^showMousePointer\033\\'

    def _stty_cooked(self) -> None:
        """
        Call 'stty' to set cooked mode.
        Actually executes '/bin/sh -c stty sane cooked < /dev/tty'

        """
        self._do_stty(False)

    def _stty_raw(self) -> None:
        """
        Call 'stty' to set raw mode.
        Actually executes '/bin/sh -c stty -ignbrk -brkint -parmrk -istrip -inlcr -igncr -icrnl
        -ixon -opost -echo -echonl -icanon -isig -iexten -parenb cs8 min 1 < /dev/tty'

        """
        self._do_stty(True)

    _STTY_CMD_RAW = [
        '/bin/sh', '-c',
        (
            'stty -ignbrk -brkint -parmrk -istrip -inlcr -igncr -icrnl -ixon -opost -echo '
            '-echonl -icanon -isig -iexten -parenb cs8 min 1 < /dev/tty'
        )
    ]

    _STTY_CMD_COOKED = ['/bin/sh', '-c', 'stty sane cooked < /dev/tty']

    def _do_stty(self, mode: bool) -> None:
        """
        Call 'stty' to set raw or cooked mode.

        mode - if true, set raw mode, otherwise set cooked mode

        """
        if mode:
            subprocess.run(self._STTY_CMD_RAW, check=True)
        else:
            subprocess.run(self._STTY_CMD_COOKED, check=True)

    def _clear_all(self) -> str:
        """
        Clear the entire screen.  Because some terminals use back-color-erase,
        set the color to white-on-black beforehand.

        returns the string to emit to an ANSI / ECMA-style terminal

        """
        return '\033[0;37;40m\033[2J'

    def _flush_string(self, out_strs: List[str]) -> str:
        """
        Render the screen to a string that can be emitted to something that
        knows how to process ECMA-48/ANSI X3.64 escape sequences.

        out_strs - list to append escape sequence to

        Returns escape sequences string that provides the updates to the physical screen

        """
        attr = None
        if self.really_cleared:
            attr = CellAttributes()
            out_strs.append(self._clear_all())

        # Draw the text part now.
        for y in sorted(self.get_dirty_rows()):
            self._flush_line(y, out_strs, attr)
        self.clear_dirty_flag()

        self.really_cleared = False
        return ''.join(out_strs)

    def _flush_line(
            self, y: int, out_strs: List[str], last_attr: Optional[CellAttributes]) -> None:
        """
        Perform a somewhat-optimal rendering of a line.

        y - row coordinate.  0 is the top-most row.
        out_strs - list to write escape sequences to
        last_attr cell attributes from the last call to _flush_line

        """
        last_x = -1

        logical_row = self.logical[y]
        physical_row = self.physical[y]

        for x, (l_cell, p_cell) in enumerate(zip(logical_row, physical_row)):
            if l_cell == p_cell and not self.really_cleared:
                continue

            # If a double width cell has been split in two or if it straddles the screen edge,
            # put a blank.
            if l_cell.width == CellWidth.LEFT:
                if x == self.width - 1 or logical_row[x+1].width != CellWidth.RIGHT:
                    l_cell = l_cell.with_update(
                        ch=graphicschars.DOUBLEWIDTHSURROGATE, width=CellWidth.SINGLE)
            if l_cell.width == CellWidth.RIGHT:
                if x == 0 or logical_row[x-1].width != CellWidth.LEFT:
                    l_cell = l_cell.with_update(
                        ch=graphicschars.DOUBLEWIDTHSURROGATE, width=CellWidth.SINGLE)

            if last_attr is None:
                last_attr = CellAttributes()
                out_strs.append(self._normal())

            # Place the cell
            if last_x != x - 1 or last_x == -1:
                out_strs.append(self._goto_x_y(x, y))

            # Physical is always updated
            physical_row[x] = l_cell

            # Don't render the right-hand side of a two-character cell.
            if l_cell.width == CellWidth.RIGHT:
                continue

            # Determine which attributes have changed
            same_fore = l_cell.attr.fore_color == last_attr.fore_color
            same_back = l_cell.attr.back_color == last_attr.back_color
            same_flags = l_cell.attr.flags == last_attr.flags

            # Now emit only the modified attributes
            if not same_fore and not same_back and same_flags:
                # Both colors changed, attributes the same
                is_bold = l_cell.attr.flags & CellFlags.BOLD != 0
                out_strs.append(self._color_bold_fore_back(
                    is_bold, l_cell.attr.fore_color, l_cell.attr.back_color
                ))
            elif not same_fore and not same_back and not same_flags:
                # Everything is different
                is_bold = l_cell.attr.flags & CellFlags.BOLD != 0
                is_reverse = l_cell.attr.flags & CellFlags.REVERSE != 0
                is_blink = l_cell.attr.flags & CellFlags.BLINK != 0
                is_underline = l_cell.attr.flags & CellFlags.UNDERLINE != 0
                out_strs.append(self._color(
                    l_cell.attr.fore_color, l_cell.attr.back_color, is_bold,
                    is_reverse, is_blink, is_underline
                ))
            elif not same_fore and same_back and same_flags:
                # Attributes same, fore_color different
                is_bold = l_cell.attr.flags & CellFlags.BOLD != 0
                out_strs.append(self._color_bold_color_foreground(
                    is_bold, l_cell.attr.fore_color, True
                ))
            elif same_fore and not same_back and same_flags:
                # Attributes same, back_color different
                is_bold = l_cell.attr.flags & CellFlags.BOLD != 0
                out_strs.append(self._color_bold_color_foreground(
                    is_bold, l_cell.attr.back_color, False
                ))
            elif l_cell.attr == last_attr:
                # All attributes the same, just print the char
                pass  # nop
            else:
                # Just reset everything again
                is_bold = l_cell.attr.flags & CellFlags.BOLD != 0
                is_reverse = l_cell.attr.flags & CellFlags.REVERSE != 0
                is_blink = l_cell.attr.flags & CellFlags.BLINK != 0
                is_underline = l_cell.attr.flags & CellFlags.UNDERLINE != 0
                out_strs.append(self._color(
                    l_cell.attr.fore_color, l_cell.attr.back_color, is_bold,
                    is_reverse, is_blink, is_underline
                ))

            # Emit the character
            out_strs.append(l_cell.ch)

            # Save the last rendered cell
            last_x = x
            last_attr = l_cell.attr

        # Update remainder of physical line
        if x < self.width - 1:
            # Physical is always updated
            for i in range(x, self.width):
                physical_row[i] = Cell()

            # Clear remaining line
            out_strs.append(self._goto_x_y(x, y))
            out_strs.append(self._clear_remaining_line())
            last_attr = CellAttributes()


_ECMA48_COLORS = {
    Color.BLACK: 0,
    Color.RED: 1,
    Color.GREEN: 2,
    Color.BROWN: 3,
    Color.BLUE: 4,
    Color.MAGENTA: 5,
    Color.CYAN: 6,
    Color.LIGHT_GRAY: 7,

    Color.DARK_GRAY: 0,
    Color.BRIGHT_RED: 1,
    Color.BRIGHT_GREEN: 2,
    Color.YELLOW: 3,
    Color.BRIGHT_BLUE: 4,
    Color.BRIGHT_MAGENTA: 5,
    Color.BRIGHT_CYAN: 6,
    Color.WHITE: 7,

    Color.SGR_BLACK: 0,
    Color.SGR_RED: 1,
    Color.SGR_GREEN: 2,
    Color.SGR_YELLOW: 3,
    Color.SGR_BLUE: 4,
    Color.SGR_MAGENTA: 5,
    Color.SGR_CYAN: 6,
    Color.SGR_WHITE: 7,

    Color.SGR_BOLD_BLACK: 0,
    Color.SGR_BOLD_RED: 1,
    Color.SGR_BOLD_GREEN: 2,
    Color.SGR_BOLD_YELLOW: 3,
    Color.SGR_BOLD_BLUE: 4,
    Color.SGR_BOLD_MAGENTA: 5,
    Color.SGR_BOLD_CYAN: 6,
    Color.SGR_BOLD_WHITE: 7,
}
