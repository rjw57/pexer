from asyncio.queues import Queue

# When type-checking we need some classes which would be a circular import if imported directly.
import pexer.backend

__all__ = ['GenericBackend']


class GenericBackend:
    """
    >>> b = _make_test_backend()
    >>> assert isinstance(b, GenericBackend)

    """
    def __init__(
            self, *, session_info: 'pexer.backend.TSessionInfo',
            screen: 'pexer.backend.LogicalScreen',
            terminal: 'pexer.backend.TerminalReader') -> None:
        self._session_info = session_info
        self._screen = screen
        self._terminal = terminal
        self._is_read_only = False
        self._is_open = False

    @property
    def session_info(self) -> 'pexer.backend.TSessionInfo':
        """
        Get a SessionInfo, which exposes text width/height, language,
        username, and other information from the communication stack.

        Returns the SessionInfo

        >>> b = _make_test_backend()
        >>> assert b.session_info is not None

        """
        return self._session_info

    @property
    def screen(self) -> 'pexer.backend.LogicalScreen':
        """
        Get a Screen, which displays the text cells to the user.

        Returns the Screen

        >>> b = _make_test_backend()
        >>> assert b.screen is not None

        """
        return self._screen

    @property
    def is_read_only(self) -> bool:
        """
        Check if backend is read-only.

        Returns true if user input events from the backend are discarded

        >>> b = _make_test_backend()
        >>> assert not b.is_read_only

        """
        return self._is_read_only

    def flush_screen(self) -> None:
        """
        Classes must provide an implementation that syncs the logical screen
        to the physical device.

        """
        self.screen.flush_physical()

    def open(self, event_queue: Queue) -> None:
        if self._is_open:
            return
        self._terminal.open(event_queue)
        self._is_open = True

    def close(self) -> None:
        """
        Classes must provide an implementation that closes sockets, restores
        console, etc.

        >>> import contextlib
        >>> with contextlib.closing(_make_test_backend()) as b:
        ...    b.open(None)
        ...    pass
        >>> b._terminal.close.assert_called()

        """
        if not self._is_open:
            return
        self._terminal.close()
        self._is_open = False

    def set_title(self, title: str) -> None:
        """
        Classes must provide an implementation that sets the window title.

        title - the new title

        >>> b = _make_test_backend()
        >>> b.set_title('foo')
        >>> b.screen.set_title.assert_called_with('foo')

        """
        self.screen.set_title(title)

    def set_is_read_only(self, read_only: bool) -> None:
        """
        Set read-only flag.

        read_only - if true, then input events will be discarded

        >>> b = _make_test_backend()
        >>> assert not b.is_read_only
        >>> b.set_is_read_only(True)
        >>> assert b.is_read_only

        """
        self._is_read_only = read_only


def _make_test_backend() -> 'GenericBackend':
    from unittest.mock import MagicMock
    return GenericBackend(session_info=MagicMock(), screen=MagicMock(), terminal=MagicMock())
