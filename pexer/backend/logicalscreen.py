import contextlib
from typing import Optional, Set, List, Iterable, Generator, Tuple

from ..bits import graphicschars, Cell, CellAttributes, stringutils, CellWidth

__all__ = ['LogicalScreen']


class LogicalScreen:
    def __init__(self) -> None:
        self.logical: List[List[Cell]] = []
        self.physical: List[List[Cell]] = []
        self._offset_x, self._offset_y = 0, 0
        self._cursor_x, self._cursor_y = 0, 0
        self._clip_left, self._clip_right, self._clip_top, self._clip_bottom = 0, 0, 0, 0
        self._width, self._height = 0, 0
        self._title = ''
        self._dirty_rows: Set[int] = set()  # set of rows which *potentially* need refreshing
        self.cursor_visible = False
        self.really_cleared = False
        self.reallocate(80, 24)
        self.clear_dirty_flag()

    @property
    def cursor_x(self) -> int:
        return self._cursor_x

    @property
    def cursor_y(self) -> int:
        return self._cursor_y

    @property
    def offset_x(self) -> int:
        return self._offset_x

    @property
    def offset_y(self) -> int:
        return self._offset_y

    @property
    def clip_left(self) -> int:
        return self._clip_left

    @property
    def clip_right(self) -> int:
        return self._clip_right

    @property
    def clip_top(self) -> int:
        return self._clip_top

    @property
    def clip_bottom(self) -> int:
        return self._clip_bottom

    @property
    def title(self) -> str:
        return self._title

    @property
    def width(self) -> int:
        return self._width

    @property
    def height(self) -> int:
        return self._height

    def set_width(self, value: int) -> None:
        """
        Set width. Everything on-screen will be destroyed and must be re-drawn.

        >>> s = LogicalScreen()
        >>> s.set_width(10)
        >>> assert s.width == 10

        """
        self.set_dimensions(value, self.height)

    def set_height(self, value: int) -> None:
        """
        Set height. Everything on-screen will be destroyed and must be re-drawn.

        >>> s = LogicalScreen()
        >>> s.set_height(10)
        >>> assert s.height == 10

        """
        self.set_dimensions(self.width, value)

    def set_offset_x(self, offset_x: int) -> None:
        self._offset_x = offset_x

    def set_offset_y(self, offset_y: int) -> None:
        self._offset_y = offset_y

    def set_clip_left(self, clip_left: int) -> None:
        self._clip_left = clip_left

    def set_clip_right(self, clip_right: int) -> None:
        self._clip_right = clip_right

    def set_clip_top(self, clip_top: int) -> None:
        self._clip_top = clip_top

    def set_clip_bottom(self, clip_bottom: int) -> None:
        self._clip_bottom = clip_bottom

    def set_cursor_x(self, value: int) -> None:
        self._cursor_x = value

    def set_cursor_y(self, value: int) -> None:
        self._cursor_y = value

    def set_dimensions(self, width: int, height: int) -> None:
        """
        Change width and height. Everything on-screen will be destroyed and must be re-drawn.

        >>> s = LogicalScreen()
        >>> s.set_dimensions(90, 40)
        >>> assert s.width == 90
        >>> assert s.height == 40

        """
        self.reallocate(width, height)
        self.resize_to_screen()

    def set_title(self, title: str) -> None:
        """
        Set the window title.

        """
        self._title = title

    @contextlib.contextmanager
    def saving_context(self) -> Generator:
        """
        Convenience context manager which saves clipping rect and transform on enter and
        restores them on exit.

        """
        cl, cr = self._clip_left, self._clip_right
        ct, cb = self._clip_top, self._clip_bottom
        ox, oy = self._offset_x, self._offset_y
        yield
        self._clip_left, self._clip_right = cl, cr
        self._clip_top, self._clip_bottom = ct, cb
        self._offset_x, self._offset_y = ox, oy

    def translate(self, x: int, y: int) -> None:
        """
        Translate the origin so that (0, 0) moves to (x, y). The clipping rectangle's absolute
        co-ordinates is updated so that it covers the same physical part of the screen.

        """
        self._clip_left -= x
        self._clip_right -= x
        self._clip_top -= y
        self._clip_bottom -= y
        self._offset_x += x
        self._offset_y += y

    def logical_to_physical(self, x: int, y: int) -> Tuple[int, int]:
        """
        Convert a logical co-ordinate to physical screen co-ordinate.

        """
        return x + self._offset_x, y + self._offset_y

    def physical_to_logical(self, x: int, y: int) -> Tuple[int, int]:
        """
        Convert a physical screen co-ordinate to logical co-ordinate.

        """
        return x - self._offset_x, y - self._offset_y

    def clip_to(self, left: int, top: int, width: int, height: int) -> None:
        """
        Update the current clipping area by intersecting it with a rectangle supplied in logical
        co-ordinates.

        """
        cl = max(self._clip_left, left)
        cr = min(self._clip_right, left + width)
        ct = max(self._clip_top, top)
        cb = min(self._clip_bottom, top + height)
        self._clip_left, self._clip_right = cl, cr
        self._clip_top, self._clip_bottom = ct, cb

    def is_dirty(self) -> bool:
        """
        Get dirty flag. If true, the logical screen has been drawn to since the last time
        self.clear_dirty_flag() was called.

        >>> s = LogicalScreen()
        >>> assert not s.is_dirty()
        >>> s.put_cell_x_y(10, 10, Cell(ch='x', attr=CellAttributes()))
        >>> assert s.is_dirty()

        """
        return len(self._dirty_rows) != 0

    def get_dirty_rows(self) -> Iterable[int]:
        """
        Return a sequence of row indexes which are dirty and need updating.

        """
        actually_dirty_rows = set()
        for row_idx in self._dirty_rows:
            if self.logical[row_idx] != self.physical[row_idx]:
                actually_dirty_rows.add(row_idx)
        return actually_dirty_rows

    def clear_dirty_flag(self) -> None:
        """
        Clear the dirty flag.

        """
        self._dirty_rows = set()

    def get_attr_x_y(self, x: int, y: int) -> CellAttributes:
        """
        Get attributes at one location.

        x - column coordinate.  0 is the left-most column.
        y - row coordinate.  0 is the top-most row.

        >>> s = LogicalScreen()
        >>> a = s.get_attr_x_y(10, 2)
        >>> assert a == s.logical[2][10].attr

        """
        try:
            return self.logical[y][x].attr
        except IndexError:
            return CellAttributes()

    def get_cell_x_y(self, x: int, y: int) -> Cell:
        """
        Get a cell at one location.

        x - column coordinate.  0 is the left-most column.
        y - row coordinate.  0 is the top-most row.

        >>> s = LogicalScreen()
        >>> a = s.get_cell_x_y(10, 2)
        >>> assert a == s.logical[2][10]

        """
        try:
            return self.logical[y][x]
        except IndexError:
            return Cell.BLANK

    def put_attr_x_y(
            self, x: int, y: int, attr: CellAttributes, *, clip: bool = True) -> None:
        """
        Set the attributes at one location.

        x - column coordinate.  0 is the left-most column.
        y - row coordinate.  0 is the top-most row.
        attr - CellAttributes to use
        clip - if true, honor clipping/offset

        >>> from ..bits import CellFlags
        >>> s = LogicalScreen()
        >>> a = CellAttributes(CellFlags.BOLD)
        >>> assert s.get_attr_x_y(5, 10) != a
        >>> s.put_attr_x_y(5, 10, a)
        >>> assert s.get_attr_x_y(5, 10) == a

        """
        if clip:
            if x < self.clip_left or x >= self.clip_right:
                return
            if y < self.clip_top or y >= self.clip_bottom:
                return
            x += self.offset_x
            y += self.offset_y

        if y < 0 or y >= len(self.logical):
            return

        row = self.logical[y]
        if x >= 0 and x < self.width and y >= 0 and y < self.height:
            if row[x].attr != attr:
                row[x] = row[x].with_update(attr=attr)
                self._dirty_rows.add(y)

    def put_all(self, ch: str, attr: CellAttributes) -> None:
        """
        Fill the entire screen with one character with attributes.

        >>> from ..bits import CellFlags
        >>> s = LogicalScreen()
        >>> s.set_dimensions(10, 5)
        >>> a = CellAttributes(CellFlags.BOLD)
        >>> s.put_all('x', a)
        >>> c = s.get_cell_x_y(4, 3)
        >>> assert c.ch == 'x'
        >>> assert c.attr == a
        >>> print(s._logical_chars())
        xxxxxxxxxx
        xxxxxxxxxx
        xxxxxxxxxx
        xxxxxxxxxx
        xxxxxxxxxx

        """
        self.draw_rect(0, 0, self.width, self.height, ch, attr)

    def put_cell_x_y(self, x: int, y: int, cell: Cell) -> None:
        """
        Render a character cell with attributes.

        Args:
            x (int): column co-ordinate. 0 is left-most
            y (int): row co-ordinate. 0 is top-most
            cell (Cell): cell to draw at that position

        """
        if x < self.clip_left or x >= self.clip_right:
            return
        if y < self.clip_top or y >= self.clip_bottom:
            return

        if cell.width == CellWidth.SINGLE and stringutils.width(cell.ch) == 2:
            return self.put_full_width_cell_x_y(x, y, cell)

        x += self.offset_x
        y += self.offset_y

        if x >= 0 and x < self.width and y >= 0 and y < self.height:
            if self.logical[y][x] != cell:
                self.logical[y][x] = cell
                self._dirty_rows.add(y)

            # If this also happens to be the cursor position, make the position dirty.
            if self.cursor_x == x and self.cursor_y == y:
                self.physical[y][x] = Cell.BLANK

    def put_full_width_cell_x_y(self, x: int, y: int, cell: Cell) -> None:
        """
        Render a cell containing a full-width char.

        Args:
            x (int): column co-ordinate with 0 being the left-most column
            y (int): row co-ordinate with 0 being the top-most row
            cell (Cell): cell to render

        """
        # Don't put a full width character if the clip rectangle would divide it in two.
        if x == self.clip_left - 1:
            self.put_cell_x_y(x+1, y, cell.with_update(
                ch=graphicschars.DOUBLEWIDTHSURROGATE, width=CellWidth.SINGLE))
            return
        if x == self.clip_right - 2:
            self.put_cell_x_y(x, y, cell.with_update(
                ch=graphicschars.DOUBLEWIDTHSURROGATE, width=CellWidth.SINGLE))
            return
        self.put_cell_x_y(x, y, cell.with_update(width=CellWidth.LEFT))
        self.put_cell_x_y(x + 1, y, cell.with_update(width=CellWidth.RIGHT))

    def put_string_x_y(
            self, x: int, y: int, s: str, attr: Optional[CellAttributes] = None) -> None:
        """
        Render a string. Does not wrap if the string exceeds the line. If *attr* is not None, it is
        CellAttributes to set characters to otherwise existing attributes are used.

        >>> from ..bits import CellFlags
        >>> s = LogicalScreen()
        >>> s.set_dimensions(15, 5)
        >>> a = CellAttributes(flags=CellFlags.BOLD)
        >>> s.put_all('X', a)
        >>> s.put_string_x_y(5, 1, 'Hello')
        >>> assert s.get_cell_x_y(6, 1).ch == 'e'
        >>> assert s.get_attr_x_y(6, 1) == a
        >>> b = CellAttributes()
        >>> assert a != b
        >>> s.put_string_x_y(5, 1, 'Hello', b)
        >>> assert s.get_attr_x_y(6, 1) == b
        >>> print(s._logical_chars())
        XXXXXXXXXXXXXXX
        XXXXXHelloXXXXX
        XXXXXXXXXXXXXXX
        XXXXXXXXXXXXXXX
        XXXXXXXXXXXXXXX

        """
        if attr is None:
            # Relatively slow version using put_cell_x_y.
            for ch, w in stringutils.str_cells(s):
                old_ch = self.get_cell_x_y(x, y)
                self.put_cell_x_y(x, y, old_ch.with_update(ch=ch))
                x += w
                if x >= self.clip_right:
                    break
        else:
            # More optimised version which doesn't test clipping for each cell.
            if y < self.clip_top or y >= self.clip_bottom:
                return

            x += self.offset_x
            y += self.offset_y
            if y < 0 or y >= self.height:
                return

            min_x = max(0, self.clip_left + self.offset_x)
            max_x = min(self.clip_right + self.offset_x, self.width)

            logical_row = self.logical[y]
            was_dirty = False
            for ch, w in stringutils.str_cells(s):
                if w == 1 and x >= min_x and x < max_x:
                    # Common case: single-width character.
                    cell = Cell(ch=ch, attr=attr)
                    if logical_row[x] != cell:
                        logical_row[x] = cell
                        was_dirty = True

                    # If this also happens to be the cursor position, make the position dirty.
                    if self.cursor_x == x and self.cursor_y == y:
                        self.physical[y][x] = Cell.BLANK
                else:
                    # Perform full-width character logic
                    self.put_cell_x_y(
                        x - self._offset_x, y - self._offset_y, Cell(ch=ch, attr=attr))
                x += w
                if x >= max_x:
                    break

            if was_dirty:
                self._dirty_rows.add(y)

    def draw_rect(
            self, x: int, y: int, width: int, height: int, ch: str, attr: CellAttributes) -> None:
        """
        Draw a rectangle with top-left at (x, y) and the specified width.

        >>> s = LogicalScreen()
        >>> s.set_dimensions(15, 5)
        >>> s.put_all('-', CellAttributes())
        >>> s.draw_rect(2, 3, 10, 2, 'x', CellAttributes())
        >>> print(s._logical_chars())
        ---------------
        ---------------
        ---------------
        --xxxxxxxxxx---
        --xxxxxxxxxx---
        >>> s.set_offset_x(1)
        >>> s.set_offset_y(2)
        >>> s.draw_rect(0, 0, 4, 3, '+', CellAttributes())
        >>> s.reset_clipping()
        >>> print(s._logical_chars())
        ---------------
        ---------------
        -++++----------
        -++++xxxxxxx---
        -++++xxxxxxx---
        >>> s.set_clip_left(1)
        >>> s.set_clip_right(12)
        >>> s.set_clip_top(1)
        >>> s.set_clip_bottom(4)
        >>> s.draw_rect(0, 0, 15, 5, '.', CellAttributes())
        >>> s.draw_rect(0, 0, 1, 1, '-', CellAttributes())
        >>> s.draw_rect(3, 0, 5, 10, '/', CellAttributes())
        >>> s.reset_clipping()
        >>> print(s._logical_chars())
        ---------------
        -../////....---
        -../////....---
        -../////....---
        -++++xxxxxxx---

        """
        # Entirely outside clipping area
        if x >= self.clip_right or y >= self.clip_bottom:
            return
        if x + width <= self.clip_left or y + height <= self.clip_top:
            return

        if x < self.clip_left:
            width -= self.clip_left - x
            x = self.clip_left
        if x + width > self.clip_right:
            width = self.clip_right - x
        if y < self.clip_top:
            height -= self.clip_top - y
            y = self.clip_top
        if y + height > self.clip_bottom:
            height = self.clip_bottom - y

        x += self.offset_x
        y += self.offset_y

        if x < 0:
            width += x
            x = 0
        if x + width >= self.width:
            width = self.width - x
        if y < 0:
            height += y
            y = 0
        if y + height >= self.height:
            height = self.height - y

        cell = Cell(ch=ch, attr=attr)
        for r in range(y, y+height):
            row = self.logical[r]
            was_dirty = False
            for c in range(x, x+width):
                if row[c] != cell:
                    row[c] = cell
                    was_dirty = True
            if was_dirty:
                self._dirty_rows.add(r)

        # If this rect also happens to contain the cursor position, make the position dirty.
        if (self.cursor_x >= x and self.cursor_x < x + width
                and self.cursor_y >= y and self.cursor_y < y + height):
            self.physical[self.cursor_y][self.cursor_x] = Cell.BLANK

    def v_line_x_y(
            self, x: int, y: int, n: int, ch: str, attr: Optional[CellAttributes] = None) -> None:
        """
        Draw a vertical line from (x, y) to (x, y+n). If *attr* is not None, it is
        CellAttributes to set characters to otherwise existing attributes are used.

        >>> s = LogicalScreen()
        >>> s.set_dimensions(20, 5)
        >>> s.put_all('-', CellAttributes())
        >>> s.v_line_x_y(4, 1, 3, 'X')
        >>> s.v_line_x_y(6, 0, 5, 'Y', CellAttributes())
        >>> print(s._logical_chars())
        ------Y-------------
        ----X-Y-------------
        ----X-Y-------------
        ----X-Y-------------
        ------Y-------------

        """
        if attr is None:
            for i in range(n):
                old_ch = self.get_cell_x_y(x, y)
                self.put_cell_x_y(x, y + i, old_ch.with_update(ch=ch))
        else:
            self.draw_rect(x, y, 1, n, ch, attr)

    def h_line_x_y(
            self, x: int, y: int, n: int, ch: str, attr: Optional[CellAttributes] = None) -> None:
        """
        Draw a horizontal line from (x, y) to (x+n, y). If *attr* is not None, it is
        CellAttributes to set characters to otherwise existing attributes are used.

        >>> s = LogicalScreen()
        >>> s.set_dimensions(20, 5)
        >>> s.put_all('-', CellAttributes())
        >>> s.h_line_x_y(4, 1, 3, 'X')
        >>> s.h_line_x_y(6, 0, 5, 'Y', CellAttributes())
        >>> print(s._logical_chars())
        ------YYYYY---------
        ----XXX-------------
        --------------------
        --------------------
        --------------------

        """
        if attr is None:
            for i in range(n):
                old_ch = self.get_cell_x_y(x, y)
                self.put_cell_x_y(x + i, y, old_ch.with_update(ch=ch))
        else:
            self.draw_rect(x, y, n, 1, ch, attr)

    def resize_to_screen(self) -> None:
        """
        Resize the physical screen to match the logical screen dimensions.

        """
        # to be overridden

    def reset(self) -> None:
        """
        Reset all screen cells to default. Flushes offset and clip values.

        >>> s = LogicalScreen()
        >>> s.put_all('X', CellAttributes())
        >>> c = Cell()
        >>> assert s.get_cell_x_y(10, 20) != c
        >>> s.reset()
        >>> assert s.get_cell_x_y(10, 20) == c

        """
        for r in self.logical:
            for i in range(len(r)):
                r[i] = Cell()
        self._dirty_rows = set(range(self.height))
        self.reset_clipping()

    def reset_clipping(self) -> None:
        self._offset_x, self._offset_y = 0, 0
        self._clip_left, self._clip_right = 0, 0
        self._clip_right, self._clip_bottom = self.width, self.height

    def clear(self) -> None:
        """
        Clear the logical screen.
        """
        self.reset()

    def draw_box(
            self, left: int, top: int, right: int, bottom: int, border: CellAttributes,
            background: CellAttributes, border_type: int = 1, shadow: bool = False) -> None:
        """
        Draw a box with a border and empty background.

        left - left column of box.  0 is the left-most column.
        top - top row of the box.  0 is the top-most row.
        right - right column of box
        bottom - bottom row of the box
        border - CellAttributes to use for the border
        background - CellAttributes to use for the background
        borderType if 1, draw a single-line border; if 2, draw a
            double-line border; if 3, draw double-line top/bottom edges and
            single-line left/right edges (like Qmodem)
        shadow - if true, draw a "shadow" on the box

        >>> s = LogicalScreen()
        >>> s.set_dimensions(20, 8)
        >>> s.put_all(':', CellAttributes())
        >>> s.draw_box(1, 1, 19, 7, CellAttributes(), CellAttributes())
        >>> s.draw_box(3, 2, 17, 6, CellAttributes(), CellAttributes(), 2)
        >>> s.draw_box(5, 3, 15, 5, CellAttributes(), CellAttributes(), 3)
        >>> print(s._logical_chars())
        ::::::::::::::::::::
        :┌────────────────┐:
        :│ ╔════════════╗ │:
        :│ ║ ╒════════╕ ║ │:
        :│ ║ ╘════════╛ ║ │:
        :│ ╚════════════╝ │:
        :└────────────────┘:
        ::::::::::::::::::::
        >>> s.put_all(':', CellAttributes())
        >>> s.set_clip_left(1)
        >>> s.set_clip_right(15)
        >>> s.set_clip_top(2)
        >>> s.set_clip_bottom(6)
        >>> s.draw_box(1, 1, 19, 7, CellAttributes(), CellAttributes())
        >>> s.draw_box(3, 2, 17, 6, CellAttributes(), CellAttributes(), 2)
        >>> s.draw_box(5, 3, 15, 5, CellAttributes(), CellAttributes(), 3, shadow=True)
        >>> print(s._logical_chars())
        ::::::::::::::::::::
        ::::::::::::::::::::
        :│ ╔═══════════:::::
        :│ ║ ╒════════╕:::::
        :│ ║ ╘════════╛:::::
        :│ ╚═══════════:::::
        ::::::::::::::::::::
        ::::::::::::::::::::

        """
        box_width, box_height = right - left, bottom - top

        if border_type == 1:
            c_top_left = graphicschars.ULCORNER
            c_top_right = graphicschars.URCORNER
            c_bottom_left = graphicschars.LLCORNER
            c_bottom_right = graphicschars.LRCORNER
            c_h_side = graphicschars.SINGLE_BAR
            c_v_side = graphicschars.WINDOW_SIDE
        elif border_type == 2:
            c_top_left = graphicschars.WINDOW_LEFT_TOP_DOUBLE
            c_top_right = graphicschars.WINDOW_RIGHT_TOP_DOUBLE
            c_bottom_left = graphicschars.WINDOW_LEFT_BOTTOM_DOUBLE
            c_bottom_right = graphicschars.WINDOW_RIGHT_BOTTOM_DOUBLE
            c_h_side = graphicschars.DOUBLE_BAR
            c_v_side = graphicschars.WINDOW_SIDE_DOUBLE
        elif border_type == 3:
            c_top_left = graphicschars.WINDOW_LEFT_TOP
            c_top_right = graphicschars.WINDOW_RIGHT_TOP
            c_bottom_left = graphicschars.WINDOW_LEFT_BOTTOM
            c_bottom_right = graphicschars.WINDOW_RIGHT_BOTTOM
            c_h_side = graphicschars.WINDOW_TOP
            c_v_side = graphicschars.WINDOW_SIDE
        else:
            raise ValueError(f'Invalid border type: {border_type}')

        # Place the corner characters
        self.put_cell_x_y(left, top, Cell(ch=c_top_left, attr=border))
        self.put_cell_x_y(left + box_width - 1, top, Cell(ch=c_top_right, attr=border))
        self.put_cell_x_y(
            left, top + box_height - 1,
            Cell(ch=c_bottom_left, attr=border))
        self.put_cell_x_y(
            left + box_width - 1, top + box_height - 1,
            Cell(ch=c_bottom_right, attr=border))

        # Draw the box lines
        self.h_line_x_y(left + 1, top, box_width - 2, c_h_side, border)
        self.v_line_x_y(left, top + 1, box_height - 2, c_v_side, border)
        self.h_line_x_y(left + 1, top + box_height - 1, box_width - 2, c_h_side, border)
        self.v_line_x_y(left + box_width - 1, top + 1, box_height - 2, c_v_side, border)

        # Fill in the interior background
        self.draw_rect(1 + left, 1 + top, box_width - 2, box_height - 2, ' ', background)

        if shadow:
            # Draw a shadow
            self.draw_box_shadow(left, top, right, bottom)

    def draw_box_shadow(self, left: int, top: int, right: int, bottom: int) -> None:
        box_top, box_left = top, left
        box_width, box_height = right - left, bottom - top
        shadow_attr = CellAttributes()

        for i in range(box_height):
            self.put_attr_x_y(box_left + box_width, box_top + 1 + i, shadow_attr)
            self.put_attr_x_y(box_left + box_width + 1, box_top + 1 + i, shadow_attr)

        for i in range(box_width):
            self.put_attr_x_y(box_left + 2 + i, box_top + box_height, shadow_attr)

    def flush_physical(self) -> None:
        # Default implementation does nothing.
        pass

    def put_cursor(self, visible: bool, x: int, y: int) -> None:
        """
        Put the cursor at (x,y).

        If *visible* is True, the cursor should be visible.

        >>> s = LogicalScreen()
        >>> s.put_cursor(True, 10, 1)
        >>> assert s.cursor_x == 10
        >>> assert s.cursor_y == 1
        >>> assert s.cursor_visible

        """
        if (self.cursor_y >= 0 and self.cursor_x >= 0 and
                self.cursor_y <= self.height - 1 and self.cursor_x <= self.width - 1):
            # Make the current cursor position dirty
            self.physical[self.cursor_y][self.cursor_x] = Cell.BLANK
        self.cursor_visible = visible
        self._cursor_x, self._cursor_y = x, y

    def hide_cursor(self) -> None:
        self.cursor_visible = False

    def reallocate(self, width: int, height: int) -> None:
        self._width, self._height = width, height

        # Fill the logical screen being rendered to and the physical screen last sent out on
        # flush() with empty cells.
        self.logical, self.physical = [], []
        for row in range(self.height):
            self.logical.append([])
            self.physical.append([])
            for col in range(self.width):
                self.logical[-1].append(Cell())
                self.physical[-1].append(Cell(ch=Cell.BLANK))

        # Set clip window
        self._clip_left, self._clip_top = 0, 0
        self._clip_right, self._clip_bottom = width, height

        # Set if the user explicitly wants to redraw everything starting with a
        # ECMATerminal.clearAll().
        self.really_cleared = True

        self._dirty_rows = set(range(self.height))

    def clear_physical(self) -> None:
        """
        Clear the physical screen.

        >>> s = LogicalScreen()
        >>> s.clear_physical()

        """
        for r in self.physical:
            for i in range(len(r)):
                r[i] = Cell.BLANK

    def invert_cell(self, x: int, y: int) -> None:
        """
        Invert the cell color at a position.

        >>> from ..bits import Color
        >>> s = LogicalScreen()
        >>> a = CellAttributes()
        >>> s.put_all('X', a)
        >>> s.invert_cell(3, 3)
        >>> b = s.get_attr_x_y(3, 3)
        >>> assert a.fore_color.invert() == b.fore_color
        >>> assert a.back_color.invert() == b.back_color

        """
        cell = self.get_cell_x_y(x, y)
        inverted_attr = CellAttributes(
            flags=cell.attr.flags,
            fore_color=cell.attr.fore_color.invert(),
            back_color=cell.attr.back_color.invert())
        self.put_cell_x_y(x, y, cell.with_update(attr=inverted_attr))

#    def set_selection(self, x0, y0, x1, y1, rectangle):
#        raise NotImplementedError()
#
#    def copy_selection(self, clipboard, x0, y0, x1, y1, rectangle):
#        raise NotImplementedError()

    def snapshot(self) -> 'LogicalScreen':
        """
        Obtain a snapshot copy of the screen.

        >>> s = LogicalScreen()
        >>> s.set_dimensions(5, 3)
        >>> s.put_all('X', CellAttributes())
        >>> s_copy = s.snapshot()
        >>> s.put_string_x_y(1, 1, 'Hi', CellAttributes())
        >>> print(s._logical_chars())
        XXXXX
        XHiXX
        XXXXX
        >>> print(s_copy._logical_chars())
        XXXXX
        XXXXX
        XXXXX

        """
        other = LogicalScreen()
        other.set_dimensions(self.width, self.height)
        for row in range(self.height):
            for col in range(self.width):
                other.logical[row][col] = self.logical[row][col]
        return other

    def _logical_chars(self) -> str:
        return '\n'.join(''.join(c.ch for c in row) for row in self.logical)
