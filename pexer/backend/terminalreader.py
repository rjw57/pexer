from asyncio import Queue
from typing import Optional

from ..event import TInputEvent

__all__ = ['TerminalReader']


class TerminalReader():
    """
    TerminalReader provides keyboard and mouse events.

    """
    def open(self, event_queue: 'Queue[TInputEvent]') -> None:
        """
        Classes must provide an implementation which opens the terminal and prepares to send input
        events to the passed queue.

        """
        raise NotImplementedError()

    def close(self) -> None:
        """
        Classes must provide an implementation that closes sockets, restores
        console, etc.

        """
        raise NotImplementedError()

    @property
    def event_queue(self) -> Optional['Queue[TInputEvent]']:
        """
        Current event queue passed to open() or None if terminal has not been opened or is closed.

        """
