import datetime

__all__ = ['TSessionInfo']


class TSessionInfo:
    """
    TSessionInfo provides a default session implementation.  The username is blank, language is
    "en_US", with a 80x24 text window.

    """
    def __init__(self, width: int = 80, height: int = 24) -> None:
        self._window_width, self._window_height = width, height
        self._start_time = datetime.datetime.utcnow()

    @property
    def start_time(self) -> datetime.datetime:
        return self._start_time

    @property
    def window_width(self) -> int:
        return self._window_width

    @property
    def window_height(self) -> int:
        return self._window_height

    @property
    def username(self) -> str:
        return ''

    @property
    def language(self) -> str:
        return 'en_US'

    def query_window_size(self) -> None:
        raise NotImplementedError()
