import datetime
import getpass
import locale
import os
from typing import Optional

from .tsessioninfo import TSessionInfo

__all__ = ['TTYSessionInfo']


class TTYSessionInfo(TSessionInfo):
    """
    TTYSessionInfo queries environment variables and the tty window size for
    the session information.  The username is taken from user.name, language
    is taken from user.language, and text window size from 'stty size'.

    """
    def __init__(self) -> None:
        super().__init__()
        self.last_query_window_time: Optional[datetime.datetime] = None
        self.query_window_size()

    @property
    def username(self) -> str:
        return getpass.getuser()

    @property
    def language(self) -> str:
        default_locale = locale.getdefaultlocale()[0]
        return default_locale if default_locale is not None else 'en_US'

    def query_window_size(self) -> None:
        if self.last_query_window_time is None:
            self.last_query_window_time = datetime.datetime.utcnow()
        else:
            if (datetime.datetime.utcnow() - self.last_query_window_time).total_seconds() < 0.1:
                # Don't re-check if it hasn't been some time since the last time
                return
        self._window_width, self._window_height = os.get_terminal_size()
