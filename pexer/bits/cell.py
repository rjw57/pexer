import collections
import enum

from typing import Optional

from .color import Color

__all__ = ['CellFlags', 'CellAttributes', 'CellWidth', 'Cell']


class CellFlags(enum.IntFlag):
    NONE = 0x00
    BOLD = 0x01
    BLINK = 0x02
    REVERSE = 0x04
    UNDERLINE = 0x08
    PROTECT = 0x10


class CellWidth(enum.Enum):
    # This cell is an entire glyph on its own.
    SINGLE = enum.auto()

    # This cell is the left half of a wide glyph.
    LEFT = enum.auto()

    # This cell is the right half of a wide glyph.
    RIGHT = enum.auto()


class CellAttributes(collections.namedtuple(
        'CellAttributes', 'flags fore_color back_color', defaults=(
            CellFlags.NONE, Color.WHITE, Color.BLACK
        ))):
    """
    Attributes of a cell which do not depend on its textual content.

    >>> a = CellAttributes()
    >>> assert a.flags == CellFlags.NONE
    >>> assert a.fore_color == Color.WHITE
    >>> assert a.back_color == Color.BLACK

    """
    def copy(self) -> 'CellAttributes':
        """
        >>> a = CellAttributes(flags=CellFlags.BOLD, fore_color=Color.RED, back_color=Color.WHITE)
        >>> assert a != CellAttributes()
        >>> b = a.copy()
        >>> assert a == b

        """
        return CellAttributes(
            flags=self.flags, fore_color=self.fore_color, back_color=self.back_color)

    def with_update(
            self, *, flags: Optional[CellFlags] = None,
            fore_color: Optional[Color] = None,
            back_color: Optional[Color] = None) -> 'CellAttributes':
        return CellAttributes(
            flags=flags if flags is not None else self.flags,
            fore_color=fore_color if fore_color is not None else self.fore_color,
            back_color=back_color if back_color is not None else self.back_color
        )


class Cell(collections.namedtuple(
        'Cell', 'ch attr width', defaults=(' ', CellAttributes(), CellWidth.SINGLE))):
    """
    A Cell represents a single cell on screen. It is immutable.

    """
    ch: str
    attr: CellAttributes
    width: CellWidth

    BLANK: 'Cell'

    def get_is_blank(self) -> bool:
        """
        Check to see if this cell has default attributes: white foreground,
        black background, no bold/blink/reverse/underline/protect, and a
        character value of ' ' (space).

        Return true if this cell has default attributes.

        >>> assert Cell().get_is_blank()
        >>> assert not Cell(ch='x').get_is_blank()

        """
        return (
            self.attr.fore_color == Color.WHITE and self.attr.back_color == Color.BLACK
            and self.attr.flags == CellFlags.NONE and self.ch == ' '
        )

    def copy(self) -> 'Cell':
        """
        >>> a_attr = CellAttributes(
        ...     flags=CellFlags.BOLD, fore_color=Color.RED, back_color=Color.WHITE)
        >>> a = Cell(ch='x', attr=a_attr)
        >>> assert a != Cell()
        >>> b = a.copy()
        >>> assert b == a

        """
        return Cell(ch=self.ch, attr=self.attr, width=self.width)

    def with_update(
            self, ch: Optional[str] = None, attr: Optional[CellAttributes] = None,
            width: Optional[CellWidth] = None) -> 'Cell':
        """
        Return a copy of this cell updating values within it.

        >>> a = Cell(ch='x', attr=CellAttributes(flags=CellFlags.BOLD))
        >>> b = a.with_update(ch='y')
        >>> assert b.ch != a.ch
        >>> assert b.attr == a.attr
        >>> a = Cell(ch='x', attr=CellAttributes(flags=CellFlags.BOLD))
        >>> b = a.with_update(attr=CellAttributes())
        >>> assert b.ch == a.ch
        >>> assert b.attr != a.attr

        """
        return Cell(
            ch=ch if ch is not None else self.ch,
            attr=attr if attr is not None else self.attr,
            width=width if width is not None else self.width
        )


Cell.BLANK = Cell(ch=65535)
