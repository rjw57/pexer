import collections

__all__ = ['Color']


class Color(collections.namedtuple('Color', 'red green blue')):
    """
    Represent a 24-bit colour as three integers in the range [0, 255].

    """
    red: int
    green: int
    blue: int

    BLACK: 'Color'
    RED: 'Color'
    GREEN: 'Color'
    BROWN: 'Color'
    BLUE: 'Color'
    MAGENTA: 'Color'
    CYAN: 'Color'
    LIGHT_GRAY: 'Color'
    DARK_GRAY: 'Color'
    BRIGHT_RED: 'Color'
    BRIGHT_GREEN: 'Color'
    YELLOW: 'Color'
    BRIGHT_BLUE: 'Color'
    BRIGHT_MAGENTA: 'Color'
    BRIGHT_CYAN: 'Color'
    WHITE: 'Color'

    SGR_BLACK: 'Color'
    SGR_RED: 'Color'
    SGR_GREEN: 'Color'
    SGR_YELLOW: 'Color'
    SGR_BLUE: 'Color'
    SGR_MAGENTA: 'Color'
    SGR_CYAN: 'Color'
    SGR_WHITE: 'Color'

    SGR_BOLD_BLACK: 'Color'
    SGR_BOLD_RED: 'Color'
    SGR_BOLD_GREEN: 'Color'
    SGR_BOLD_YELLOW: 'Color'
    SGR_BOLD_BLUE: 'Color'
    SGR_BOLD_MAGENTA: 'Color'
    SGR_BOLD_CYAN: 'Color'
    SGR_BOLD_WHITE: 'Color'

    @staticmethod
    def from_integer(v: int) -> 'Color':
        """
        Convert an integer RGB color into a Color.

        """
        return Color((v >> 16) & 0xff, (v >> 8) & 0xff, v & 0xff)

    @staticmethod
    def from_hex_string(string: str) -> 'Color':
        """
        Convert a hex string into a color.

        >>> Color.from_hex_string('54a800')
        Color(red=84, green=168, blue=0)

        """
        return Color.from_integer(int(string, 16))

    def invert(self) -> 'Color':
        """
        Invert a color in the same way as (CGA/VGA color XOR 0x7).

        >>> assert Color.BLACK.invert() == Color.WHITE
        >>> assert Color.WHITE.invert() == Color.BLACK

        """
        return Color(
            max(0, min(0xff, 0xfc - self.red)),
            max(0, min(0xff, 0xfc - self.green)),
            max(0, min(0xff, 0xfc - self.blue))
        )

    def brighten(self) -> 'Color':
        """
        Brighten a color.

        """
        return Color(
            min(0xff, self.red + 0x54),
            min(0xff, self.green + 0x54),
            min(0xff, self.blue + 0x54),
        )


Color.BLACK = Color(0x00, 0x00, 0x00)
Color.RED = Color(0xa8, 0x00, 0x00)
Color.GREEN = Color(0x00, 0xa8, 0x00)
Color.BROWN = Color(0xa8, 0x54, 0x00)
Color.BLUE = Color(0x00, 0x00, 0xa8)
Color.MAGENTA = Color(0xa8, 0x00, 0xa8)
Color.CYAN = Color(0x00, 0xa8, 0xa8)
Color.LIGHT_GRAY = Color(0xa8, 0xa8, 0xa8)
Color.DARK_GRAY = Color(0x54, 0x54, 0x54)
Color.BRIGHT_RED = Color(0xfc, 0x54, 0x54)
Color.BRIGHT_GREEN = Color(0x54, 0xfc, 0x54)
Color.YELLOW = Color(0xfc, 0xfc, 0x54)
Color.BRIGHT_BLUE = Color(0x54, 0x54, 0xfc)
Color.BRIGHT_MAGENTA = Color(0xfc, 0x54, 0xfc)
Color.BRIGHT_CYAN = Color(0x54, 0xfc, 0xfc)
Color.WHITE = Color(0xfc, 0xfc, 0xfc)

Color.SGR_BLACK = Color(0x00, 0x00, 0x00)
Color.SGR_RED = Color(0xa8, 0x00, 0x00)
Color.SGR_GREEN = Color(0x00, 0xa8, 0x00)
Color.SGR_YELLOW = Color(0xa8, 0x54, 0x00)
Color.SGR_BLUE = Color(0x00, 0x00, 0xa8)
Color.SGR_MAGENTA = Color(0xa8, 0x00, 0xa8)
Color.SGR_CYAN = Color(0x00, 0xa8, 0xa8)
Color.SGR_WHITE = Color(0xa8, 0xa8, 0xa8)

Color.SGR_BOLD_BLACK = Color(0x54, 0x54, 0x54)
Color.SGR_BOLD_RED = Color(0xfc, 0x54, 0x54)
Color.SGR_BOLD_GREEN = Color(0x54, 0xfc, 0x54)
Color.SGR_BOLD_YELLOW = Color(0xfc, 0xfc, 0x54)
Color.SGR_BOLD_BLUE = Color(0x54, 0x54, 0xfc)
Color.SGR_BOLD_MAGENTA = Color(0xfc, 0x54, 0xfc)
Color.SGR_BOLD_CYAN = Color(0x54, 0xfc, 0xfc)
Color.SGR_BOLD_WHITE = Color(0xfc, 0xfc, 0xfc)
