import collections
from typing import Dict

from .cell import CellAttributes, CellFlags
from .color import Color

__all__ = ['ColorTheme']


class ColorTheme(collections.UserDict):
    """
    ColorTheme is a collection of colors keyed by string. A default theme is also provided that
    matches the blue-and-white theme used by Turbo Vision.

    """
    data: Dict[str, CellAttributes]

    def __init__(self) -> None:
        super().__init__()
        self.set_default_theme()

    def set_default_theme(self) -> None:
        """
        Sets to defaults that resemble the Borland IDE colors.

        """
        # TWindow border
        color = CellAttributes(
                fore_color=Color.SGR_BOLD_WHITE, back_color=Color.SGR_BLUE, flags=CellFlags.BOLD)
        self.data['twindow.border'] = color

        # TWindow background
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_YELLOW, back_color=Color.SGR_BLUE, flags=CellFlags.BOLD)
        self.data['twindow.background'] = color

        # TWindow border - inactive
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_BLACK, back_color=Color.SGR_BLUE, flags=CellFlags.BOLD)
        self.data['twindow.border.inactive'] = color

        # TWindow background - inactive
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_YELLOW, back_color=Color.SGR_BLUE, flags=CellFlags.BOLD)
        self.data['twindow.background.inactive'] = color

        # TWindow border - modal
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_WHITE, back_color=Color.SGR_WHITE, flags=CellFlags.BOLD)
        self.data['twindow.border.modal'] = color

        # TWindow background - modal
        color = CellAttributes(fore_color=Color.SGR_BLACK, back_color=Color.SGR_WHITE)
        self.data['twindow.background.modal'] = color

        # TWindow border - modal + inactive
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_BLACK, back_color=Color.SGR_WHITE, flags=CellFlags.BOLD)
        self.data['twindow.border.modal.inactive'] = color

        # TWindow background - modal + inactive
        color = CellAttributes(fore_color=Color.SGR_BLACK, back_color=Color.SGR_WHITE)
        self.data['twindow.background.modal.inactive'] = color

        # TWindow border - during window movement - modal
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_GREEN, back_color=Color.SGR_WHITE, flags=CellFlags.BOLD)
        self.data['twindow.border.modal.windowmove'] = color

        # TWindow border - during window movement
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_GREEN, back_color=Color.SGR_BLUE, flags=CellFlags.BOLD)
        self.data['twindow.border.windowmove'] = color

        # TWindow background - during window movement
        color = CellAttributes(fore_color=Color.SGR_YELLOW, back_color=Color.SGR_BLUE)
        self.data['twindow.background.windowmove'] = color

        # TDesktop background
        color = CellAttributes(fore_color=Color.SGR_BLUE, back_color=Color.SGR_WHITE)
        self.data['tdesktop.background'] = color

        # TButton text
        color = CellAttributes(fore_color=Color.SGR_BLACK, back_color=Color.SGR_GREEN)
        self.data['tbutton.inactive'] = color
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_CYAN, back_color=Color.SGR_GREEN, flags=CellFlags.BOLD)
        self.data['tbutton.active'] = color
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_BLACK, back_color=Color.SGR_WHITE, flags=CellFlags.BOLD)
        self.data['tbutton.disabled'] = color
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_YELLOW, back_color=Color.SGR_GREEN, flags=CellFlags.BOLD)
        self.data['tbutton.mnemonic'] = color
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_YELLOW, back_color=Color.SGR_GREEN, flags=CellFlags.BOLD)
        self.data['tbutton.mnemonic.highlighted'] = color

        # TLabel text
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_WHITE, back_color=Color.SGR_BLUE, flags=CellFlags.BOLD)
        self.data['tlabel'] = color
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_YELLOW, back_color=Color.SGR_BLUE, flags=CellFlags.BOLD)
        self.data['tlabel.mnemonic'] = color

        # TText text
        color = CellAttributes(fore_color=Color.SGR_WHITE, back_color=Color.SGR_BLUE)
        self.data['ttext'] = color

        # TField text
        color = CellAttributes(fore_color=Color.SGR_BLACK, back_color=Color.SGR_WHITE)
        self.data['tfield.inactive'] = color
        color = CellAttributes(fore_color=Color.SGR_BLACK, back_color=Color.SGR_CYAN)
        self.data['tfield.active'] = color

        # TCheckBox
        color = CellAttributes(fore_color=Color.SGR_WHITE, back_color=Color.SGR_BLUE)
        self.data['tcheckbox.inactive'] = color
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_YELLOW, back_color=Color.SGR_BLACK, flags=CellFlags.BOLD)
        self.data['tcheckbox.active'] = color
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_YELLOW, back_color=Color.SGR_BLUE, flags=CellFlags.BOLD)
        self.data['tcheckbox.mnemonic'] = color
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_RED, back_color=Color.SGR_BLACK, flags=CellFlags.BOLD)
        self.data['tcheckbox.mnemonic.highlighted'] = color

        # TComboBox
        color = CellAttributes(fore_color=Color.SGR_BLACK, back_color=Color.SGR_WHITE)
        self.data['tcombobox.inactive'] = color
        color = CellAttributes(fore_color=Color.SGR_BLUE, back_color=Color.SGR_CYAN)
        self.data['tcombobox.active'] = color

        # TSpinner
        color = CellAttributes(fore_color=Color.SGR_BLACK, back_color=Color.SGR_WHITE)
        self.data['tspinner.inactive'] = color
        color = CellAttributes(fore_color=Color.SGR_BLUE, back_color=Color.SGR_CYAN)
        self.data['tspinner.active'] = color

        # TCalendar
        color = CellAttributes(fore_color=Color.SGR_WHITE, back_color=Color.SGR_BLUE)
        self.data['tcalendar.background'] = color
        color = CellAttributes(fore_color=Color.SGR_WHITE, back_color=Color.SGR_BLUE)
        self.data['tcalendar.day'] = color
        color = CellAttributes(fore_color=Color.SGR_RED, back_color=Color.SGR_WHITE)
        self.data['tcalendar.day.selected'] = color
        color = CellAttributes(fore_color=Color.SGR_BLUE, back_color=Color.SGR_CYAN)
        self.data['tcalendar.arrow'] = color
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_WHITE, back_color=Color.SGR_BLUE, flags=CellFlags.BOLD)
        self.data['tcalendar.title'] = color

        # TRadioButton
        color = CellAttributes(fore_color=Color.SGR_WHITE, back_color=Color.SGR_BLUE)
        self.data['tradiobutton.inactive'] = color
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_YELLOW, back_color=Color.SGR_BLACK, flags=CellFlags.BOLD)
        self.data['tradiobutton.active'] = color
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_YELLOW, back_color=Color.SGR_BLUE, flags=CellFlags.BOLD)
        self.data['tradiobutton.mnemonic'] = color
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_RED, back_color=Color.SGR_BLACK, flags=CellFlags.BOLD)
        self.data['tradiobutton.mnemonic.highlighted'] = color

        # TRadioGroup
        color = CellAttributes(fore_color=Color.SGR_WHITE, back_color=Color.SGR_BLUE)
        self.data['tradiogroup.inactive'] = color
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_YELLOW, back_color=Color.SGR_BLUE, flags=CellFlags.BOLD)
        self.data['tradiogroup.active'] = color

        # TMenu
        color = CellAttributes(fore_color=Color.SGR_BLACK, back_color=Color.SGR_WHITE)
        self.data['tmenu'] = color
        color = CellAttributes(fore_color=Color.SGR_BLACK, back_color=Color.SGR_GREEN)
        self.data['tmenu.highlighted'] = color
        color = CellAttributes(fore_color=Color.SGR_RED, back_color=Color.SGR_WHITE)
        self.data['tmenu.mnemonic'] = color
        color = CellAttributes(fore_color=Color.SGR_RED, back_color=Color.SGR_GREEN)
        self.data['tmenu.mnemonic.highlighted'] = color
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_BLACK, back_color=Color.SGR_WHITE, flags=CellFlags.BOLD)
        self.data['tmenu.disabled'] = color

        # TProgressBar
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_BLUE, back_color=Color.SGR_BLUE, flags=CellFlags.BOLD)
        self.data['tprogressbar.complete'] = color
        color = CellAttributes(fore_color=Color.SGR_WHITE, back_color=Color.SGR_BLUE)
        self.data['tprogressbar.incomplete'] = color

        # THScroller / TVScroller
        color = CellAttributes(fore_color=Color.SGR_CYAN, back_color=Color.SGR_BLUE)
        self.data['tscroller.bar'] = color
        color = CellAttributes(fore_color=Color.SGR_BLUE, back_color=Color.SGR_CYAN)
        self.data['tscroller.arrows'] = color

        # TTreeView
        color = CellAttributes(fore_color=Color.SGR_WHITE, back_color=Color.SGR_BLUE)
        self.data['ttreeview'] = color
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_GREEN, back_color=Color.SGR_BLUE, flags=CellFlags.BOLD)
        self.data['ttreeview.expandbutton'] = color
        color = CellAttributes(fore_color=Color.SGR_BLACK, back_color=Color.SGR_CYAN)
        self.data['ttreeview.selected'] = color
        color = CellAttributes(fore_color=Color.SGR_RED, back_color=Color.SGR_BLUE)
        self.data['ttreeview.unreadable'] = color
        color = CellAttributes(fore_color=Color.SGR_WHITE, back_color=Color.SGR_BLUE)
        self.data['ttreeview.inactive'] = color
        color = CellAttributes(fore_color=Color.SGR_BLACK, back_color=Color.SGR_WHITE)
        self.data['ttreeview.selected.inactive'] = color

        # TList
        color = CellAttributes(fore_color=Color.SGR_WHITE, back_color=Color.SGR_BLUE)
        self.data['tlist'] = color
        color = CellAttributes(fore_color=Color.SGR_BLACK, back_color=Color.SGR_CYAN)
        self.data['tlist.selected'] = color
        color = CellAttributes(fore_color=Color.SGR_BLACK, back_color=Color.SGR_CYAN)
        self.data['tlist.unreadable'] = color
        color = CellAttributes(fore_color=Color.SGR_WHITE, back_color=Color.SGR_BLUE)
        self.data['tlist.inactive'] = color
        color = CellAttributes(fore_color=Color.SGR_BLACK, back_color=Color.SGR_WHITE)
        self.data['tlist.selected.inactive'] = color

        # TStatusBar
        color = CellAttributes(fore_color=Color.SGR_BLACK, back_color=Color.SGR_WHITE)
        self.data['tstatusbar.text'] = color
        color = CellAttributes(fore_color=Color.SGR_RED, back_color=Color.SGR_WHITE)
        self.data['tstatusbar.button'] = color
        color = CellAttributes(fore_color=Color.SGR_WHITE, back_color=Color.SGR_BLUE)
        self.data['tstatusbar.selected'] = color

        # TEditor
        color = CellAttributes(fore_color=Color.SGR_WHITE, back_color=Color.SGR_BLUE)
        self.data['teditor'] = color
        color = CellAttributes(fore_color=Color.SGR_BLACK, back_color=Color.SGR_CYAN)
        self.data['teditor.selected'] = color
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_WHITE, back_color=Color.SGR_RED, flags=CellFlags.BOLD)
        self.data['teditor.margin'] = color

        # TTable
        color = CellAttributes(fore_color=Color.SGR_WHITE, back_color=Color.SGR_BLUE)
        self.data['ttable.inactive'] = color
        color = CellAttributes(fore_color=Color.SGR_BLACK, back_color=Color.SGR_CYAN)
        self.data['ttable.active'] = color
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_YELLOW, back_color=Color.SGR_CYAN, flags=CellFlags.BOLD)
        self.data['ttable.selected'] = color
        color = CellAttributes(fore_color=Color.SGR_BLACK, back_color=Color.SGR_WHITE)
        self.data['ttable.label'] = color
        color = CellAttributes(fore_color=Color.SGR_BLUE, back_color=Color.SGR_WHITE)
        self.data['ttable.label.selected'] = color
        color = CellAttributes(fore_color=Color.SGR_WHITE, back_color=Color.SGR_BLUE)
        self.data['ttable.border'] = color

        # TSplitPane
        color = CellAttributes(fore_color=Color.SGR_WHITE, back_color=Color.SGR_BLUE)
        self.data['tsplitpane'] = color

        # THelpWindow border - during window movement
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_GREEN, back_color=Color.SGR_CYAN, flags=CellFlags.BOLD)
        self.data['thelpwindow.windowmove'] = color

        # THelpWindow border
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_GREEN, back_color=Color.SGR_CYAN, flags=CellFlags.BOLD)
        self.data['thelpwindow.border'] = color

        # THelpWindow background
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_WHITE, back_color=Color.SGR_CYAN, flags=CellFlags.BOLD)
        self.data['thelpwindow.background'] = color

        # THelpWindow text
        color = CellAttributes(fore_color=Color.SGR_WHITE, back_color=Color.SGR_BLUE)
        self.data['thelpwindow.text'] = color

        # THelpWindow link
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_YELLOW, back_color=Color.SGR_BLUE, flags=CellFlags.BOLD)
        self.data['thelpwindow.link'] = color

        # THelpWindow link - active
        color = CellAttributes(
            fore_color=Color.SGR_BOLD_YELLOW, back_color=Color.SGR_CYAN, flags=CellFlags.BOLD)
        self.data['thelpwindow.link.active'] = color
