import re

__all__ = ['MnemonicString']


class MnemonicString:
    """
    MnemonicString is used to render a string like "&File" into a highlighted 'F' and the rest of
    'ile'.  To insert a literal '&', use two '&&' characters, e.g. "&File &&
    Stuff" would be "File & Stuff" with the first 'F' highlighted.

    """
    def __init__(self, label: str) -> None:
        # Find first '&[^&]' if present
        self._screen_shortcut_idx = -1
        self._shortcut = ''
        shortcut_match = re.search('&([^&])', label)
        if shortcut_match:
            self._screen_shortcut_idx = shortcut_match.start(0)
            self._shortcut = shortcut_match.group(1)
            label = re.sub('&([^&])', '\\1', label, count=1)

        # Replace '&&' with '&'
        label = re.sub('&&', '&', label)
        self._raw_label = label

    def get_raw_label(self) -> str:
        return self._raw_label

    def get_screen_shortcut_idx(self) -> int:
        return self._screen_shortcut_idx

    def get_shortcut(self) -> str:
        return self._shortcut
