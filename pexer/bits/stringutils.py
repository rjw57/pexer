import functools

import wcwidth  # type: ignore
from typing import Iterator, Tuple

__all__ = ['width']


@functools.lru_cache(maxsize=None)
def width(s: str) -> int:
    """
    Determine display width of a string.

    """
    w = wcwidth.wcswidth(s)

    # Unlike wcwidth, we fall back to number of code points if the width is indeterminate.
    return w if w >= 0 else len(s)


def str_cells(s: str) -> Iterator[Tuple[str, int]]:
    """
    A generator which generates (text, width) pairs for each glyph in the string giving the
    textual representation and the width in character cells.

    """
    slice_start, w = 0, 0
    for idx, c in enumerate(s):
        c_w = max(width(c), 0)
        if c_w > 0 and w > 0:
            yield s[slice_start:idx], w
            slice_start = idx
            w = 0
        w += c_w

    if w > 0:
        yield s[slice_start:], w
