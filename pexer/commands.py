"""
Standard TCommand-s.

"""
from .tcommand import TCommand

#: Immediately abort the application (e.g. remote side closed connection).
ABORT = TCommand.from_string('ABORT')

#: File open dialog.
OPEN = TCommand.from_string('OPEN')

#: Exit application.
EXIT = TCommand.from_string('EXIT')

#: Spawn OS shell window.
SHELL = TCommand.from_string('SHELL')

#: Cut selected text and copy to the clipboard.
CUT = TCommand.from_string('CUT')

#: Copy selected text to clipboard.
COPY = TCommand.from_string('COPY')

#: Paste from clipboard.
PASTE = TCommand.from_string('PASTE')

#: Clear selected text without copying it to the clipboard.
CLEAR = TCommand.from_string('CLEAR')

#: Enter help system.
HELP = TCommand.from_string('HELP')

#: Enter first menu.
MENU = TCommand.from_string('MENU')

#: Save file.
SAVE = TCommand.from_string('SAVE')

#: Tile windows
WINDOW_TILE = TCommand.from_string(f'{__name__}.window_tile')

#: Cascade windows
WINDOW_CASCADE = TCommand.from_string(f'{__name__}.window_cascade')

#: Close all open windows
WINDOW_CLOSE_ALL = TCommand.from_string(f'{__name__}.window_close_all')

#: Switch to keyboard move/size for current window.
WINDOW_MOVE = TCommand.from_string(f'{__name__}.window_move')

#: Zoom/restore current window.
WINDOW_ZOOM = TCommand.from_string(f'{__name__}.window_zoom')

#: Switch to next window.
WINDOW_NEXT = TCommand.from_string(f'{__name__}.window_next')

#: Switch to previous window.
WINDOW_PREVIOUS = TCommand.from_string(f'{__name__}.window_previous')

#: Close current window.
WINDOW_CLOSE = TCommand.from_string(f'{__name__}.window_close')
