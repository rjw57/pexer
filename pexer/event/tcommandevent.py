from ..tcommand import TCommand
from .tinputevent import TInputEvent

# When type-checking we need some classes which would be a circular import if imported directly.
import pexer.backend

__all__ = ['TCommandEvent']


class TCommandEvent(TInputEvent):
    cmd: TCommand

    def __init__(self, backend: 'pexer.backend.GenericBackend', cmd: TCommand) -> None:
        """
        Public constructor.

        backend - the backend that generated this event
        cmd - the TCommand or integer representing the command

        >>> from pexer import TCommand, commands
        >>> e = TCommandEvent(None, commands.ABORT)

        """
        super().__init__(backend)
        self.cmd = cmd
