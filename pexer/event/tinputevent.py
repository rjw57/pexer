import datetime

# When type-checking we need some classes which would be a circular import if imported directly.
import pexer.backend

__all__ = ['TInputEvent']


class TInputEvent:
    """
    This is the parent class of all events dispatched to the UI.

    """
    backend: 'pexer.backend.GenericBackend'
    time: datetime.datetime

    def __init__(self, backend: 'pexer.backend.GenericBackend') -> None:
        # Backend which generated the event
        self.backend = backend

        # Time at which event was generated (UTC)
        self.time = datetime.datetime.utcnow()
