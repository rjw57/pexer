from ..backend.genericbackend import GenericBackend
from ..tkeypress import TKeypress

from .tinputevent import TInputEvent

__all__ = ['TKeypressEvent']


class TKeypressEvent(TInputEvent):
    is_key: bool
    fn_key: int
    ch: int
    alt: bool
    ctrl: bool
    shift: bool

    def __init__(
            self, backend: GenericBackend, is_key: bool, fn_key: int, ch: int,
            alt: bool, ctrl: bool, shift: bool) -> None:
        """
        Public constructor.

        backend - the backend that generated this event
        is_key - is true, this is a function key
        fn_key - the function key code (only valid if is_key is true)
        ch - the character code point (only valid if fn_key is false)
        alt - if true, ALT was pressed with this keystroke
        ctrl - if true, CTRL was pressed with this keystroke
        shift - if true, SHIFT was pressed with this keystroke

        >>> e = TKeypressEvent(None, False, False, ord('x'), False, False, False)

        """
        super().__init__(backend)
        self.key = TKeypress(is_key, fn_key, ch, alt, ctrl, shift)
