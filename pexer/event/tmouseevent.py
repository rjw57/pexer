import enum

from ..backend.genericbackend import GenericBackend
from . import TInputEvent

__all__ = ['TMouseEvent']


class TMouseEvent(TInputEvent):
    class Type(enum.Enum):
        # Mouse motion.  X and Y will have screen coordinates.
        MOUSE_MOTION = enum.auto()

        # Mouse button down.  X and Y will have screen coordinates.
        MOUSE_DOWN = enum.auto()

        # Mouse button up.  X and Y will have screen coordinates.
        MOUSE_UP = enum.auto()

        # Mouse double-click.  X and Y will have screen coordinates.
        MOUSE_DOUBLE_CLICK = enum.auto()

    type: Type
    x: int
    y: int
    absolute_x: int
    absolute_y: int
    mouse1: bool
    mouse2: bool
    mouse3: bool
    mouse_wheel_up: bool
    mouse_wheel_down: bool
    alt: bool
    ctrl: bool
    shift: bool

    def __init__(
            self, backend: GenericBackend, type_: Type, x: int, y: int,
            absolute_x: int, absolute_y: int, mouse1: bool, mouse2: bool, mouse3: bool,
            mouse_wheel_up: bool, mouse_wheel_down: bool,
            alt: bool, ctrl: bool, shift: bool) -> None:
        """
        Public constructor.

        backend - the backend that generated this event
        type_ - the Type of mouse, Type.Screen or Type.Widget
        x - relative column
        y - relative row
        absolute_x - absolute column
        absolute_y - absolute row
        mouse1 - if true, left button is down
        mouse2 - if true, right button is down
        mouse3 - if true, middle button is down
        mouse_wheel_up - if true, mouse wheel (button 4) is down
        mouse_wheel_down - if true, mouse wheel (button 5) is down
        alt - if true, ALT was pressed with this mouse event
        ctrl - if true, CTRL was pressed with this mouse event
        shift - if true, SHIFT was pressed with this mouse event

        """
        super().__init__(backend)
        if not isinstance(type_, TMouseEvent.Type):
            raise ValueError('type_ must be a TMouseEvent.Type')
        self.type = type_
        self.x, self.y = x, y
        self.absolute_x, self.absolute_y = absolute_x, absolute_y
        self.mouse1, self.mouse2, self.mouse3 = mouse1, mouse2, mouse3
        self.mouse_wheel_up, self.mouse_wheel_down = mouse_wheel_up, mouse_wheel_down
        self.alt, self.ctrl, self.shift = alt, ctrl, shift
