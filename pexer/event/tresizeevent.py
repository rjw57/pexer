import enum

from ..backend.genericbackend import GenericBackend
from . import TInputEvent

__all__ = ['TResizeEvent']


class TResizeEvent(TInputEvent):
    class Type(enum.Enum):
        # The entire screen size changed
        SCREEN = enum.auto()

        # A widget was resized
        WIDGET = enum.auto()

    type: Type
    width: int
    height: int

    def __init__(self, backend: GenericBackend, type_: Type, width: int, height: int) -> None:
        """
        Public constructor.

        backend - the backend that generated this event
        type_ - the Type of resize, Type.Screen or Type.Widget
        width - the new width
        height - the new height

        >>> from unittest.mock import MagicMock
        >>> e = TResizeEvent(MagicMock(), TResizeEvent.Type.WIDGET, 4, 5)
        >>> e = TResizeEvent(MagicMock(), 'WIDGET', 4, 5)
        Traceback (most recent call last):
            ...
        ValueError: type_ must be a TResizeEvent.Type

        """
        super().__init__(backend)
        if not isinstance(type_, TResizeEvent.Type):
            raise ValueError('type_ must be a TResizeEvent.Type')
        self.type = type_
        self.width, self.height = width, height
