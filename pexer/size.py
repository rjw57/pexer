from typing import Optional, NamedTuple

__all__ = ['TOptionalSize', 'TSize']


class TOptionalSize(NamedTuple):
    """
    A size with optional width and height. A "don't care" value is signalled by None.

    """
    width: Optional[int] = None
    height: Optional[int] = None


class TSize(NamedTuple):
    """
    A size.

    """
    width: int = 0
    height: int = 0
