import asyncio
import contextlib
import logging
import traceback
from typing import cast, Awaitable, Callable, Optional, List

from . import commands
from .tcommand import TCommand
from .tdesktop import TDesktop
from .backend import ECMA48Backend
from .bits import Cell, ColorTheme
from .event import TKeypressEvent, TMouseEvent, TInputEvent, TCommandEvent, TResizeEvent
from .size import TSize

# When type-checking we need some classes which would be a circular import if imported directly.
import pexer
import pexer.backend

__all__ = ['TApplication', 'TAction']

LOG = logging.getLogger(__name__)

TAction = Callable[[], Optional[Awaitable[None]]]


class TApplication:
    """
    TApplication is the main driver class for a full Text User Interface
    application.

    """
    def __init__(self) -> None:
        self._backend: 'pexer.backend.GenericBackend' = ECMA48Backend()
        self._event_queue: Optional[asyncio.Queue[TInputEvent]] = None
        self._event_consumer_task: Optional[asyncio.Task] = None
        self._is_running = False
        self._call_later_queue: Optional[asyncio.Queue[TAction]] = None
        self._call_later_backlog: List[TAction] = []
        self._draw_lock: Optional[asyncio.Lock] = None
        self._flush_screen_event: Optional[asyncio.Event] = None
        self._hide_mouse_when_typing = True

        # Mouse cursor location and old drawn location
        self._mouse_x, self._mouse_y = 0, 0
        self._old_drawn_mouse_x, self._old_drawn_mouse_y = 0, 0

        # Old drawn mouse cell
        self._old_drawn_mouse_cell = Cell()

        # Draw mouse cursor
        self._text_mouse = True

        # Mouse cursor is hidden due to typing
        self._typing_hid_mouse = False

        # Flag indicating if we're expecting to perform a re-paint of the screen "soon".
        self._expecting_repaint = False

        # Color theme for the application.
        self.theme = ColorTheme()

        # Root widget
        self._root_widget: 'pexer.TWidget' = TDesktop()
        self._root_widget.set_direct_application(self)

    @property
    def backend(self) -> 'pexer.backend.GenericBackend':
        return self._backend

    @property
    def screen(self) -> 'pexer.backend.LogicalScreen':
        return self.backend.screen

    @property
    def is_running(self) -> bool:
        return self._is_running

    @property
    def text_mouse(self) -> bool:
        return self._text_mouse

    def set_text_mouse(self, text_mouse: bool) -> None:
        self._text_mouse = text_mouse

    @property
    def root_widget(self) -> 'pexer.TWidget':
        return self._root_widget

    def set_root_widget(self, widget: 'pexer.TWidget') -> None:
        prev_root = self.root_widget
        prev_root.set_direct_application(None)
        widget.set_direct_application(self)
        widget.set_parent(None)
        self._root_widget = widget

    @property
    def window_stack(self) -> 'pexer.TWindowStack':
        """
        Window stack for application. Can be used to add windows.

        """
        try:
            return cast(TDesktop, self._root_widget).window_stack
        except AttributeError:
            raise ValueError('root widget has no window stack')

    def add_window(self, window: 'pexer.TWindow') -> None:
        """
        Convenience method to add a window to the window stack.

        """
        self.window_stack.add_window(window)

    def remove_window(self, window: 'pexer.TWindow') -> None:
        """
        Convenience method to remove a window from the window stack.

        """
        self.window_stack.remove_window(window)

    def raise_window(self, window: 'pexer.TWindow') -> None:
        """
        Convenience method to raise a window to the top of the window stack.

        """
        self.window_stack.raise_window(window)

    def do_repaint(self) -> None:
        if not self._expecting_repaint:
            self.call_later(self._perform_redraw, skip_repaint=True)

    def post_input_event(self, event: TInputEvent) -> None:
        """
        Post a TInputEvent subclass into the event queue.

        """
        if self._event_queue is None:
            return
        self._event_queue.put_nowait(event)

    def post_command(self, command: TCommand) -> None:
        """
        Post a TCommand to the application.

        """
        self.post_input_event(TCommandEvent(self.backend, command))

    def on_command(self, command: TCommandEvent) -> bool:
        """
        Method that TApplication subclasses can override to handle menu or posted command events.

        command - TCommandEvent event

        Returns true if this event was consumed.

        """
        cmd = command.cmd
        if cmd == commands.EXIT:
            self.exit()
            return True

        return False

    def on_resize(self, size: TSize) -> None:
        """
        Called when a resize event had been received. Sub-classes should make sure to call the
        superclass implementation.

        """
        self._root_widget.set_size(size)

    def on_start(self) -> None:
        """
        Subclasses can use this hook to create resources after the event loop has been initialised.
        Do *NOT* create widgets in the __init__ method of a subclass.

        """

    def on_exit(self) -> None:
        """
        Subclasses can use this hook to cleanup resources.

        """

    def on_keypress(self, keypress: TKeypressEvent) -> bool:
        """
        Method that TApplication subclasses can override to handle keystrokes.

        Args:
            keypress: Key press event

        Returns:
            True if the event has been handled.

        """
        return False

    def on_pre_draw(self) -> None:
        """
        Method called immediately before the screen is drawn.

        """

    def on_post_draw(self) -> None:
        """
        Method called immediately after the screen is drawn.

        """

    def exit(self) -> None:
        """
        Force this application to exit.

        """
        if not self.is_running:
            pass  # nop
        if self._event_consumer_task is not None:
            self._event_consumer_task.cancel()
        self.on_exit()

    def run(self) -> None:
        """
        Run this application until it exits.

        """
        if self.is_running:
            pass  # nop
        asyncio.run(self.run_async())

    def perform_action(self, action: TAction) -> Optional[asyncio.Task]:
        """
        Perform an action callable. If action is an ordinary callable, it is immediately called
        with the return value being returned from this method. If it is a coroutine function, it is
        scheduled for later execution via asyncio.create_task and the Task is returned.

        """
        if asyncio.iscoroutinefunction(action):
            task_coro = cast(Awaitable[None], action)
            assert task_coro is not None
            return asyncio.create_task(task_coro)
        action()
        return None

    def call_later(self, callable_: TAction, *, skip_repaint: bool = False) -> None:
        """
        Place a Python callable on the run queue, and run it asynchronously. Supports ordinary
        Python callables or coroutine functions.

        Args:
            callable_ (callable or coroutine): callable to call later
            skip_repaint (bool): if True, do not call self.do_repaint() after

        """
        wrapped_callable = self._wrap_callable(callable_, skip_repaint=skip_repaint)

        if self._call_later_queue is not None:
            self._call_later_queue.put_nowait(wrapped_callable)
        else:
            self._call_later_backlog.append(wrapped_callable)

    async def run_async(self) -> None:
        """
        Asynchronous run loop.

        """
        self._is_running = True
        self._event_queue = asyncio.Queue()
        self.backend.open(self._event_queue)
        with contextlib.closing(self.backend):
            # Once the backend has been opened we know the terminal size. Set the root widget size.
            self._root_widget.set_size(TSize(
                width=self.screen.width, height=self.screen.height
            ))

            # Create initial resources
            self.on_start()

            # Make call later queue
            self._call_later_queue = asyncio.Queue()
            for cb in self._call_later_backlog:
                self._call_later_queue.put_nowait(cb)
            self._call_later_backlog = []

            # Create redraw lock
            self._draw_lock = asyncio.Lock()

            # Create flush event
            self._flush_screen_event = asyncio.Event()

            # Spin up asynchronous tasks
            self._event_consumer_task = asyncio.create_task(self._event_consumer())
            call_later_task = asyncio.create_task(self._call_later_loop())
            flush_screen_task = asyncio.create_task(self._flush_screen_loop())

            # Schedule a paint
            self.do_repaint()

            # Wait for event consumer to finish
            await self._event_consumer_task

            # Clear up
            call_later_task.cancel()
            flush_screen_task.cancel()
            self._flush_screen_event = None
            self._event_consumer_task = None
            self._call_later_queue = None
            self._draw_lock = None
        self._is_running = False

    def handle_event(self, event: TInputEvent) -> None:
        """
        Handle an incoming event.

        """
        # Handle hiding mouse when typing.
        if isinstance(event, TKeypressEvent) and self._hide_mouse_when_typing:
            self._typing_hid_mouse = True

        # Peek at mouse position
        if isinstance(event, TMouseEvent):
            self._typing_hid_mouse = False

        # Handle events depending on their type
        if isinstance(event, TResizeEvent) and event.type == TResizeEvent.Type.SCREEN:
            self.screen.set_dimensions(event.width, event.height)
            self.on_resize(TSize(width=event.width, height=event.height))
            return
        elif isinstance(event, TKeypressEvent):
            if self.on_keypress(event):
                return
        elif isinstance(event, TMouseEvent):
            self._typing_hid_mouse = False
            self._mouse_x, self._mouse_y = event.x, event.y
        elif isinstance(event, TCommandEvent):
            if self.on_command(event):
                return

        # If we get this far, let the root widget have a crack.
        if self.root_widget is not None:
            self.root_widget.on_input_event(event)

    async def _call_later_loop(self) -> None:
        """
        Asynchronous loop running callables passed to call_later().

        """
        try:
            while True:
                if self._call_later_queue is None:
                    break
                cb = await self._call_later_queue.get()
                if asyncio.iscoroutinefunction(cb):
                    coro = cast(Awaitable[None], cb())
                    assert coro is not None
                    asyncio.create_task(coro)
                else:
                    cb()
                self._call_later_queue.task_done()
        except asyncio.CancelledError:
            return
        except Exception:
            self.backend.close()
            traceback.print_exc()
            self.exit()

    async def _flush_screen_loop(self) -> None:
        """
        Asynchronous loop flushing screen when it is dirty.

        """
        try:
            while True:
                if self._flush_screen_event is None:
                    break
                await self._flush_screen_event.wait()
                self._flush_screen_event.clear()
                if self._draw_lock is None:
                    break
                async with self._draw_lock:
                    self.backend.flush_screen()

                # Minimum wait before flushing again
                await asyncio.sleep(1.0 / 100.0)
        except asyncio.CancelledError:
            return
        except Exception:
            self.backend.close()
            traceback.print_exc()
            self.exit()

    async def _event_consumer(self) -> None:
        """
        Event consumer coroutine.

        """
        try:
            while True:
                # Process all pending events
                if self._event_queue is None:
                    break
                event: Optional[TInputEvent] = await self._event_queue.get()

                self._expecting_repaint = True
                while event is not None:
                    self.handle_event(event)
                    self._event_queue.task_done()

                    if not self._event_queue.empty():
                        event = await self._event_queue.get()
                    else:
                        event = None
                await self._perform_redraw()
        except asyncio.CancelledError:
            return
        except Exception:
            self.backend.close()
            traceback.print_exc()
            self.exit()

    async def _perform_redraw(self) -> None:
        if self._draw_lock is None or self._flush_screen_event is None:
            return
        async with self._draw_lock:
            self._expecting_repaint = False
            self.on_pre_draw()
            self._draw_all()
            if self.screen.is_dirty():
                self.on_post_draw()
                self._flush_screen_event.set()

    def _draw_all(self) -> None:
        """
        Redraw the screen

        """
        screen = self.screen
        assert screen is not None

        self._root_widget.draw(screen)

        # Draw the mouse cursor
        if self._text_mouse and not self._typing_hid_mouse:
            self._draw_text_mouse(self._mouse_x, self._mouse_y)

        # TOFO: screen.put_cursor or hide it
        screen.hide_cursor()

    def _draw_text_mouse(self, x: int, y: int) -> None:
        assert self.screen is not None
        self.screen.invert_cell(x, y)

    def _wrap_callable(self, callable_: TAction, skip_repaint: bool = False) -> TAction:
        """
        Wrap a callable or coroutine function so that self.do_repaint() is called once the calable
        returns or the coroutine function is await-ed.

        Args:
            callable_ (callable or coroutine): callable to wrap
            skip_repaint (bool): if True, do not call self.do_repaint() after

        Returns:
            callable: wrapped callable

        """
        async def wrapper() -> None:
            if asyncio.iscoroutinefunction(callable_):
                cb = cast(Callable[[], Awaitable[None]], callable_)
                assert cb is not None
                await cb()
            else:
                callable_()
            if not skip_repaint:
                await self._perform_redraw()
        return wrapper
