import enum
import math
from typing import Any, Optional, Tuple

from .size import TOptionalSize, TSize
from .tpoint import TPoint
from .twidget import TWidget

__all__ = ['TBoxDirection', 'TBox']


class TBoxDirection(enum.Enum):
    """
    TBoxDirection specifies the direction a TBox is laid out.

    """
    ROW = enum.auto()
    COLUMN = enum.auto()


class TBox(TWidget):
    """
    A TBox lays out child widgets in either a row or column. It behaves roughly like a CSS flex box
    with justify-content of start and flex-direction as specified at construction.

    The child widgets themselves specify grow and shrink share by means of the packing properties
    *shrink* and *grow*.

    """
    def __init__(
            self, *,
            direction: TBoxDirection = TBoxDirection.COLUMN,
            **kwargs: Any) -> None:
        self._direction = direction
        super().__init__(**kwargs)
        self._recompute_layout()

    @property
    def direction(self) -> TBoxDirection:
        return self._direction

    def set_direction(self, direction: TBoxDirection) -> None:
        self._direction = direction
        self._recompute_layout()

    def set_size(self, size: TSize) -> None:
        super().set_size(size)
        self._recompute_layout()

    def preferred_size(self, hint: TOptionalSize) -> TOptionalSize:
        _, cross_axis_hint = self._from_toptionalsize(hint)
        along, _, _ = self._compute_pref_min_max_axis_size(cross_axis_hint)
        return self._make_toptionalsize(along, cross_axis_hint)

    def minimum_size(self, hint: TOptionalSize) -> TOptionalSize:
        _, cross_axis_hint = self._from_toptionalsize(hint)
        _, along, _ = self._compute_pref_min_max_axis_size(cross_axis_hint)
        return self._make_toptionalsize(along, cross_axis_hint)

    def maximum_size(self, hint: TOptionalSize) -> TOptionalSize:
        _, cross_axis_hint = self._from_toptionalsize(hint)
        _, _, along = self._compute_pref_min_max_axis_size(cross_axis_hint)
        return self._make_toptionalsize(along, cross_axis_hint)

    def on_children_changed(self) -> None:
        super().on_children_changed()
        self._recompute_layout()

    def _recompute_layout(self) -> None:
        # Compute minimum, maximum and preferred sizes
        along_size, cross_axis_size = self._from_tsize(self.size)
        pref_along, min_along, max_along = self._compute_pref_min_max_axis_size(cross_axis_size)

        # Compute slack which is the difference between our actual size and our preferred size. A
        # negative slack means we have to shring, a positive means we need to grow. We clamp our
        # actual size according to our minimum and maximums.
        along_size = max(min_along, along_size)
        if max_along is not None:
            along_size = min(max_along, along_size)
        slack = along_size - pref_along

        # Get total grow and shrink for children.
        total_grow = sum(c.grow for c in self.children)
        total_shrink = sum(c.shrink for c in self.children)

        # Compute layout.
        child_position = 0
        for child in self.children:
            child_along, _ = self._from_toptionalsize(
                child.preferred_size(self._make_toptionalsize(None, cross_axis_size)))

            if child_along is None:
                child_along = 0
            if slack == 0:
                child.set_size(self._make_tsize(child_along, cross_axis_size))
            elif slack < 0:
                share = math.floor(-slack * (child.shrink / total_shrink))
                child.set_size(self._make_tsize(child_along - share, cross_axis_size))
                slack += share
                total_shrink -= share
            else:
                share = math.floor(slack * (child.grow / total_grow))
                child.set_size(self._make_tsize(child_along + share, cross_axis_size))
                slack += share
                total_grow -= share

            if self.direction == TBoxDirection.COLUMN:
                child.set_position(TPoint(0, child_position))
            else:
                child.set_position(TPoint(child_position, 0))
            advance, _ = self._from_tsize(child.size)
            child_position += advance

    def _compute_pref_min_max_axis_size(
            self, cross_axis_hint: Optional[int]) -> Tuple[int, int, Optional[int]]:
        """
        Return a tuple containing the preferred, minimum and maximum along-axis sizes given a
        cross-axis size hint. The maximum size may be "None" indicating that the widget can grow
        without bound.

        """
        preferred, minimum = 0, 0
        maximum: Optional[int] = 0

        for child in self.children:
            pref_along, _ = self._from_toptionalsize(
                child.preferred_size(self._make_toptionalsize(None, cross_axis_hint)))
            preferred += pref_along if pref_along is not None else 0
            if child.shrink > 0:
                min_along, _ = self._from_toptionalsize(
                    child.minimum_size(self._make_toptionalsize(None, cross_axis_hint)))
                minimum += min_along if min_along is not None else 0
            else:
                minimum += pref_along if pref_along is not None else 0
            if maximum is not None:
                if child.grow > 0:
                    max_along, _ = self._from_toptionalsize(
                        child.maximum_size(self._make_toptionalsize(None, cross_axis_hint)))
                    if max_along is None:
                        maximum = None
                    else:
                        maximum += max_along
                else:
                    maximum += pref_along if pref_along is not None else 0

        return preferred, minimum, maximum

    def _from_tsize(self, size: TSize) -> Tuple[int, int]:
        """
        Return the along-axis and cross-axis sizes from a TSize.

        """
        if self.direction == TBoxDirection.ROW:
            return size.width, size.height
        else:
            return size.height, size.width

    def _from_toptionalsize(self, size: TOptionalSize) -> Tuple[Optional[int], Optional[int]]:
        """
        Return the along-axis and cross-axis sizes from a TOptionalSize.

        """
        if self.direction == TBoxDirection.ROW:
            return size.width, size.height
        else:
            return size.height, size.width

    def _make_tsize(self, axis: int, cross_axis: int) -> TSize:
        """
        Make a size converting the axis and cross axit ro width and height as appropriate.

        """
        if self.direction == TBoxDirection.ROW:
            return TSize(width=axis, height=cross_axis)
        else:
            return TSize(height=axis, width=cross_axis)

    def _make_toptionalsize(self, axis: Optional[int], cross_axis: Optional[int]) -> TOptionalSize:
        """
        Make a optional size converting the axis and cross axit ro width and height as appropriate.

        """
        if self.direction == TBoxDirection.ROW:
            return TOptionalSize(width=axis, height=cross_axis)
        else:
            return TOptionalSize(height=axis, width=cross_axis)
