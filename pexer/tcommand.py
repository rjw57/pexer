from typing import Set, Any

import collections

__all__ = ['TCommand']


class TCommand(collections.UserString):
    """
    A TCommand acts just like a Python string except that all TCommand instances are unique and no
    TCommand instance will compare equal to any other or any string.

    >>> c1 = TCommand.from_string('copy')
    >>> repr(c1)
    "TCommand(value='copy')"
    >>> c2 = TCommand.from_string('copy')
    >>> str(c2)
    'copy'
    >>> c3 = TCommand.from_string('paste')
    >>> str(c3)
    'paste'

    >>> assert c1 is c2 and c1 == c2 and c1 != c3

    >>> assert c1 != 'copy'

    """
    _COMMANDS: Set['TCommand'] = set()

    def __init__(self, value: str):
        if any(c.data == value for c in TCommand._COMMANDS):
            raise ValueError(f'Duplicate command value: {value!r}')
        self.data = value
        TCommand._COMMANDS.add(self)

    @staticmethod
    def from_string(value: str) -> 'TCommand':
        for c in TCommand._COMMANDS:
            if c.value == value:
                return c
        return TCommand(value)

    @property
    def value(self) -> str:
        """
        >>> c = TCommand.from_string('paste')
        >>> assert c.value == 'paste'

        """
        return self.data

    def __hash__(self) -> int:
        """
        >>> c = TCommand.from_string('copy')
        >>> s = set()
        >>> assert c not in s
        >>> s.add(c)
        >>> assert c in s

        """
        return self.data.__hash__()

    def __eq__(self, other: Any) -> bool:
        """
        >>> c1, c2 = TCommand.from_string('copy'), TCommand.from_string('copy')
        >>> assert c1 == c2

        """
        return other is self

    def __ne__(self, other: Any) -> bool:
        """
        >>> c1, c2 = TCommand.from_string('copy'), TCommand.from_string('paste')
        >>> assert c1 != c2

        """
        return not self.__eq__(other)

    def __repr__(self) -> str:
        """
        >>> c = TCommand.from_string('copy')
        >>> assert repr(c) == "TCommand(value='copy')"

        """
        return f'TCommand(value={self.data!r})'

    def __str__(self) -> str:
        """
        >>> c = TCommand.from_string('copy')
        >>> assert str(c) == 'copy'

        """
        return self.value
