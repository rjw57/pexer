from typing import Any

from .backend import LogicalScreen
from .size import TOptionalSize
from .tbox import TBox
from .twindow import TWindowStack
from .twidget import TWidget

__all__ = ['TDesktop']


class TDesktop(TBox):
    """
    TDesktop is usually the root widget of an application.

    """
    def __init__(self, **kwargs: Any):
        super().__init__(**kwargs)
        self.pack(Line())
        self._window_stack = TWindowStack(fill_attributes='tdesktop.background', grow=1)
        self.pack(self._window_stack)
        self.pack(Line())
        self.set_active_child(self._window_stack)

    @property
    def window_stack(self) -> TWindowStack:
        return self._window_stack


class Line(TWidget):
    def preferred_size(self, hint: TOptionalSize) -> TOptionalSize:
        return TOptionalSize(width=hint.width, height=1)

    def minimum_size(self, hint: TOptionalSize) -> TOptionalSize:
        return TOptionalSize(width=1, height=1)

    def maximum_size(self, hint: TOptionalSize) -> TOptionalSize:
        return TOptionalSize(width=None, height=1)

    def draw(self, screen: LogicalScreen) -> None:
        super().draw(screen)

        app = self.application
        assert app is not None

        attr = app.theme['tstatusbar.text']
        screen.draw_rect(0, 0, self.size.width, self.size.height, ' ', attr)
        screen.put_string_x_y(2, 0, "This is a test", attr)
