from typing import Any, Union

from .twidget import TWidget
from .backend import LogicalScreen
from .bits import CellAttributes
from .bits.graphicschars import HATCH

__all__ = ['TFill']


class TFill(TWidget):
    """
    TFill fills its drawable area with a single character and character attributes. The attributes
    can either be a CellAttributes instance or a string corresponding to a key in the current
    theme.

    """
    def __init__(
            self, *,
            fill_character: str = HATCH,
            fill_attributes: Union[CellAttributes, str] = CellAttributes(),
            **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self._fill_character = fill_character
        self._fill_attributes = fill_attributes

    @property
    def fill_character(self) -> str:
        return self._fill_character

    def set_fill_character(self, fill_character: str) -> None:
        self._fill_character = fill_character

    @property
    def fill_attributes(self) -> Union[CellAttributes, str]:
        return self._fill_attributes

    def set_fill_attributes(self, fill_attributes: Union[CellAttributes, str]) -> None:
        self._fill_attributes = fill_attributes

    def draw(self, screen: LogicalScreen) -> None:
        fill_attr = self.fill_attributes
        if isinstance(fill_attr, str):
            app = self.application
            assert app is not None
            fill_attr = app.theme[fill_attr]
        assert isinstance(fill_attr, CellAttributes)

        screen.draw_rect(
            0, 0, self.size.width, self.size.height, self.fill_character, fill_attr)

        super().draw(screen)
