import collections

__all__ = ['TKeypress']


class TKeypress(collections.namedtuple(
        'TKeypress', 'is_function_key key_code ch alt ctrl shift')):
    """
    Public constructor makes an immutable instance.

    is_key is true, this is a function key
    fn_key the function key code (only valid if is_key is true)
    ch the character code point (only valid if fn_key is false)
    alt if true, TKeypress.ALT was pressed with this keystroke
    ctrl if true, TKeypress.CTRL was pressed with this keystroke
    shift if true, TKeypress.SHIFT was pressed with this keystroke

    >>> k = TKeypress(False, 0, ord('A'), False, False, False)
    >>> print(repr(k))
    TKeypress(is_function_key=False, key_code=0, ch=65, alt=False, ctrl=False, shift=False)
    >>> print(TKeypress.F1)
    1
    >>> print(repr(TKeypress.KB_NO_KEY))
    TKeypress(is_function_key=True, key_code=255, ch=32, alt=False, ctrl=False, shift=False)
    >>> print(repr(TKeypress.KB_CTRL_SHIFT_DOWN))
    TKeypress(is_function_key=True, key_code=33, ch=32, alt=False, ctrl=True, shift=True)
    >>> kp = TKeypress(is_function_key=True, key_code=33, ch=32, alt=False, ctrl=True, shift=True)
    >>> assert kp == TKeypress.KB_CTRL_SHIFT_DOWN
    >>> print(repr(TKeypress.KB_ALT_A.to_string()))
    'Alt+A'
    >>> print(repr(str(TKeypress.KB_ALT_A)))
    'Alt+A'

    """

    is_function_key: bool
    key_code: int
    ch: int
    alt: bool
    ctrl: bool
    shift: bool

    def to_string(self) -> str:
        """
        Make human-readable description of this TKeypress

        Returns:
            str: string displayable on the screen

        """
        if self == TKeypress.KB_ENTER:
            # Special case: Enter is "<arrow> <line> <angle>"
            return '\u25C0\u2500\u2518'
        elif self == TKeypress.KB_SPACE:
            # Special case: Space is "Space"
            return 'Space'
        elif self == TKeypress.KB_BACKSPACE:
            # Special case: Backspace is "<arrow>"
            return '\u2190'
        elif self == TKeypress.KB_BACKSPACE_DEL:
            # Special case: BackspaceDel is "<house>"
            return '\u2302'
        elif self == TKeypress.KB_SHIFT_LEFT:
            return 'Shift+\u2190'
        elif self == TKeypress.KB_SHIFT_RIGHT:
            return 'Shift+\u2192'

        modifiers = ''.join([
            'Ctrl+' if self.ctrl else '', 'Alt+' if self.alt else '',
            'Shift+' if self.shift else ''
        ])

        if self.is_function_key:
            if self.key_code == TKeypress.F1:
                return f'{modifiers}F1'
            elif self.key_code == TKeypress.F2:
                return f'{modifiers}F2'
            elif self.key_code == TKeypress.F3:
                return f'{modifiers}F3'
            elif self.key_code == TKeypress.F4:
                return f'{modifiers}F4'
            elif self.key_code == TKeypress.F5:
                return f'{modifiers}F5'
            elif self.key_code == TKeypress.F6:
                return f'{modifiers}F6'
            elif self.key_code == TKeypress.F7:
                return f'{modifiers}F7'
            elif self.key_code == TKeypress.F8:
                return f'{modifiers}F8'
            elif self.key_code == TKeypress.F9:
                return f'{modifiers}F9'
            elif self.key_code == TKeypress.F10:
                return f'{modifiers}F10'
            elif self.key_code == TKeypress.F11:
                return f'{modifiers}F11'
            elif self.key_code == TKeypress.F12:
                return f'{modifiers}F12'
            elif self.key_code == TKeypress.HOME:
                return f'{modifiers}Home'
            elif self.key_code == TKeypress.END:
                return f'{modifiers}End'
            elif self.key_code == TKeypress.END:
                return f'{modifiers}End'
            elif self.key_code == TKeypress.PGUP:
                return f'{modifiers}PgUp'
            elif self.key_code == TKeypress.PGDN:
                return f'{modifiers}PgDn'
            elif self.key_code == TKeypress.INS:
                return f'{modifiers}Ins'
            elif self.key_code == TKeypress.DEL:
                return f'{modifiers}Del'
            elif self.key_code == TKeypress.RIGHT:
                return f'{modifiers}Right'
            elif self.key_code == TKeypress.LEFT:
                return f'{modifiers}Left'
            elif self.key_code == TKeypress.UP:
                return f'{modifiers}Up'
            elif self.key_code == TKeypress.DOWN:
                return f'{modifiers}Down'
            elif self.key_code == TKeypress.TAB:
                return f'{modifiers}Tab'
            elif self.key_code == TKeypress.BTAB:
                return f'{modifiers}BTab'
            elif self.key_code == TKeypress.ENTER:
                return f'{modifiers}Enter'
            elif self.key_code == TKeypress.ESC:
                return f'{modifiers}Esc'
            elif self.key_code == TKeypress.BACKSPACE:
                return f'{modifiers}Backspace'
            elif self.key_code == TKeypress.RIGHT:
                return f'{modifiers}Right'
            elif self.key_code == TKeypress.RIGHT:
                return f'{modifiers}Right'
            else:
                return '--UNKNOWN--'
        else:
            return f'{modifiers}{chr(self.ch).upper()}'

    def to_lower_case(self) -> 'TKeypress':
        """
        Return a copy of the key in lower case. Function, alt and ctrl keys are not converted.

        >>> assert TKeypress.KB_SHIFT_F1.to_lower_case() == TKeypress.KB_SHIFT_F1
        >>> assert TKeypress.KB_ALT_SHIFT_T.to_lower_case() == TKeypress.KB_ALT_SHIFT_T
        >>> kp = TKeypress(False, 0, ord('X'), False, False, True)
        >>> kp
        TKeypress(is_function_key=False, key_code=0, ch=88, alt=False, ctrl=False, shift=True)
        >>> kp.to_lower_case()
        TKeypress(is_function_key=False, key_code=0, ch=120, alt=False, ctrl=False, shift=False)

        """
        new_shift, new_ch = self.shift, self.ch
        if (not self.is_function_key and self.ch >= ord('A') and self.ch <= ord('Z')
                and not self.ctrl and not self.alt):
            new_shift = False
            new_ch += 32
        return TKeypress(
            self.is_function_key, self.key_code, new_ch, self.alt, self.ctrl, new_shift)

    def __str__(self) -> str:
        return self.to_string()

    # "No key".
    NONE = 255

    # Function key F1.
    F1 = 1

    # Function key F2.
    F2 = 2

    # Function key F3.
    F3 = 3

    # Function key F4.
    F4 = 4

    # Function key F5.
    F5 = 5

    # Function key F6.
    F6 = 6

    # Function key F7.
    F7 = 7

    # Function key F8.
    F8 = 8

    # Function key F9.
    F9 = 9

    # Function key F10.
    F10 = 10

    # Function key F11.
    F11 = 11

    # Function key F12.
    F12 = 12

    # Home.
    HOME = 20

    # End.
    END = 21

    # Page up.
    PGUP = 22

    # Page down.
    PGDN = 23

    # Insert.
    INS = 24

    # Delete.
    DEL = 25

    # Right arrow.
    RIGHT = 30

    # Left arrow.
    LEFT = 31

    # Up arrow.
    UP = 32

    # Down arrow.
    DOWN = 33

    # Tab.
    TAB = 40

    # Back-tab (shift-tab).
    BTAB = 41

    # Enter.
    ENTER = 42

    # Escape.
    ESC = 43

    # Backspace, used for control-backspace.
    BACKSPACE = 44

    KB_NO_KEY: 'TKeypress'
    KB_F1: 'TKeypress'
    KB_F2: 'TKeypress'
    KB_F3: 'TKeypress'
    KB_F4: 'TKeypress'
    KB_F5: 'TKeypress'
    KB_F6: 'TKeypress'
    KB_F7: 'TKeypress'
    KB_F8: 'TKeypress'
    KB_F9: 'TKeypress'
    KB_F10: 'TKeypress'
    KB_F11: 'TKeypress'
    KB_F12: 'TKeypress'
    KB_ALT_F1: 'TKeypress'
    KB_ALT_F2: 'TKeypress'
    KB_ALT_F3: 'TKeypress'
    KB_ALT_F4: 'TKeypress'
    KB_ALT_F5: 'TKeypress'
    KB_ALT_F6: 'TKeypress'
    KB_ALT_F7: 'TKeypress'
    KB_ALT_F8: 'TKeypress'
    KB_ALT_F9: 'TKeypress'
    KB_ALT_F10: 'TKeypress'
    KB_ALT_F11: 'TKeypress'
    KB_ALT_F12: 'TKeypress'
    KB_CTRL_F1: 'TKeypress'
    KB_CTRL_F2: 'TKeypress'
    KB_CTRL_F3: 'TKeypress'
    KB_CTRL_F4: 'TKeypress'
    KB_CTRL_F5: 'TKeypress'
    KB_CTRL_F6: 'TKeypress'
    KB_CTRL_F7: 'TKeypress'
    KB_CTRL_F8: 'TKeypress'
    KB_CTRL_F9: 'TKeypress'
    KB_CTRL_F10: 'TKeypress'
    KB_CTRL_F11: 'TKeypress'
    KB_CTRL_F12: 'TKeypress'
    KB_SHIFT_F1: 'TKeypress'
    KB_SHIFT_F2: 'TKeypress'
    KB_SHIFT_F3: 'TKeypress'
    KB_SHIFT_F4: 'TKeypress'
    KB_SHIFT_F5: 'TKeypress'
    KB_SHIFT_F6: 'TKeypress'
    KB_SHIFT_F7: 'TKeypress'
    KB_SHIFT_F8: 'TKeypress'
    KB_SHIFT_F9: 'TKeypress'
    KB_SHIFT_F10: 'TKeypress'
    KB_SHIFT_F11: 'TKeypress'
    KB_SHIFT_F12: 'TKeypress'
    KB_ENTER: 'TKeypress'
    KB_TAB: 'TKeypress'
    KB_ESC: 'TKeypress'
    KB_HOME: 'TKeypress'
    KB_END: 'TKeypress'
    KB_PG_UP: 'TKeypress'
    KB_PG_DN: 'TKeypress'
    KB_INS: 'TKeypress'
    KB_DEL: 'TKeypress'
    KB_UP: 'TKeypress'
    KB_DOWN: 'TKeypress'
    KB_LEFT: 'TKeypress'
    KB_RIGHT: 'TKeypress'
    KB_ALT_ENTER: 'TKeypress'
    KB_ALT_TAB: 'TKeypress'
    KB_ALT_ESC: 'TKeypress'
    KB_ALT_HOME: 'TKeypress'
    KB_ALT_END: 'TKeypress'
    KB_ALT_PG_UP: 'TKeypress'
    KB_ALT_PG_DN: 'TKeypress'
    KB_ALT_INS: 'TKeypress'
    KB_ALT_DEL: 'TKeypress'
    KB_ALT_UP: 'TKeypress'
    KB_ALT_DOWN: 'TKeypress'
    KB_ALT_LEFT: 'TKeypress'
    KB_ALT_RIGHT: 'TKeypress'
    KB_CTRL_ENTER: 'TKeypress'
    KB_CTRL_TAB: 'TKeypress'
    KB_CTRL_ESC: 'TKeypress'
    KB_CTRL_HOME: 'TKeypress'
    KB_CTRL_END: 'TKeypress'
    KB_CTRL_PG_UP: 'TKeypress'
    KB_CTRL_PG_DN: 'TKeypress'
    KB_CTRL_INS: 'TKeypress'
    KB_CTRL_DEL: 'TKeypress'
    KB_CTRL_UP: 'TKeypress'
    KB_CTRL_DOWN: 'TKeypress'
    KB_CTRL_LEFT: 'TKeypress'
    KB_CTRL_RIGHT: 'TKeypress'
    KB_SHIFT_ENTER: 'TKeypress'
    KB_SHIFT_TAB: 'TKeypress'
    KB_BACK_TAB: 'TKeypress'
    KB_SHIFT_ESC: 'TKeypress'
    KB_SHIFT_HOME: 'TKeypress'
    KB_SHIFT_END: 'TKeypress'
    KB_SHIFT_PG_UP: 'TKeypress'
    KB_SHIFT_PG_DN: 'TKeypress'
    KB_SHIFT_INS: 'TKeypress'
    KB_SHIFT_DEL: 'TKeypress'
    KB_SHIFT_UP: 'TKeypress'
    KB_SHIFT_DOWN: 'TKeypress'
    KB_SHIFT_LEFT: 'TKeypress'
    KB_SHIFT_RIGHT: 'TKeypress'
    KB_A: 'TKeypress'
    KB_B: 'TKeypress'
    KB_C: 'TKeypress'
    KB_D: 'TKeypress'
    KB_E: 'TKeypress'
    KB_F: 'TKeypress'
    KB_G: 'TKeypress'
    KB_H: 'TKeypress'
    KB_I: 'TKeypress'
    KB_J: 'TKeypress'
    KB_K: 'TKeypress'
    KB_L: 'TKeypress'
    KB_M: 'TKeypress'
    KB_N: 'TKeypress'
    KB_O: 'TKeypress'
    KB_P: 'TKeypress'
    KB_Q: 'TKeypress'
    KB_R: 'TKeypress'
    KB_S: 'TKeypress'
    KB_T: 'TKeypress'
    KB_U: 'TKeypress'
    KB_V: 'TKeypress'
    KB_W: 'TKeypress'
    KB_X: 'TKeypress'
    KB_Y: 'TKeypress'
    KB_Z: 'TKeypress'
    KB_SPACE: 'TKeypress'
    KB_ALT_A: 'TKeypress'
    KB_ALT_B: 'TKeypress'
    KB_ALT_C: 'TKeypress'
    KB_ALT_D: 'TKeypress'
    KB_ALT_E: 'TKeypress'
    KB_ALT_F: 'TKeypress'
    KB_ALT_G: 'TKeypress'
    KB_ALT_H: 'TKeypress'
    KB_ALT_I: 'TKeypress'
    KB_ALT_J: 'TKeypress'
    KB_ALT_K: 'TKeypress'
    KB_ALT_L: 'TKeypress'
    KB_ALT_M: 'TKeypress'
    KB_ALT_N: 'TKeypress'
    KB_ALT_O: 'TKeypress'
    KB_ALT_P: 'TKeypress'
    KB_ALT_Q: 'TKeypress'
    KB_ALT_R: 'TKeypress'
    KB_ALT_S: 'TKeypress'
    KB_ALT_T: 'TKeypress'
    KB_ALT_U: 'TKeypress'
    KB_ALT_V: 'TKeypress'
    KB_ALT_W: 'TKeypress'
    KB_ALT_X: 'TKeypress'
    KB_ALT_Y: 'TKeypress'
    KB_ALT_Z: 'TKeypress'
    KB_ALT0: 'TKeypress'
    KB_ALT1: 'TKeypress'
    KB_ALT2: 'TKeypress'
    KB_ALT3: 'TKeypress'
    KB_ALT4: 'TKeypress'
    KB_ALT5: 'TKeypress'
    KB_ALT6: 'TKeypress'
    KB_ALT7: 'TKeypress'
    KB_ALT8: 'TKeypress'
    KB_ALT9: 'TKeypress'
    KB_CTRL_A: 'TKeypress'
    KB_CTRL_B: 'TKeypress'
    KB_CTRL_C: 'TKeypress'
    KB_CTRL_D: 'TKeypress'
    KB_CTRL_E: 'TKeypress'
    KB_CTRL_F: 'TKeypress'
    KB_CTRL_G: 'TKeypress'
    KB_CTRL_H: 'TKeypress'
    KB_CTRL_I: 'TKeypress'
    KB_CTRL_J: 'TKeypress'
    KB_CTRL_K: 'TKeypress'
    KB_CTRL_L: 'TKeypress'
    KB_CTRL_M: 'TKeypress'
    KB_CTRL_N: 'TKeypress'
    KB_CTRL_O: 'TKeypress'
    KB_CTRL_P: 'TKeypress'
    KB_CTRL_Q: 'TKeypress'
    KB_CTRL_R: 'TKeypress'
    KB_CTRL_S: 'TKeypress'
    KB_CTRL_T: 'TKeypress'
    KB_CTRL_U: 'TKeypress'
    KB_CTRL_V: 'TKeypress'
    KB_CTRL_W: 'TKeypress'
    KB_CTRL_X: 'TKeypress'
    KB_CTRL_Y: 'TKeypress'
    KB_CTRL_Z: 'TKeypress'
    KB_ALT_SHIFT_A: 'TKeypress'
    KB_ALT_SHIFT_B: 'TKeypress'
    KB_ALT_SHIFT_C: 'TKeypress'
    KB_ALT_SHIFT_D: 'TKeypress'
    KB_ALT_SHIFT_E: 'TKeypress'
    KB_ALT_SHIFT_F: 'TKeypress'
    KB_ALT_SHIFT_G: 'TKeypress'
    KB_ALT_SHIFT_H: 'TKeypress'
    KB_ALT_SHIFT_I: 'TKeypress'
    KB_ALT_SHIFT_J: 'TKeypress'
    KB_ALT_SHIFT_K: 'TKeypress'
    KB_ALT_SHIFT_L: 'TKeypress'
    KB_ALT_SHIFT_M: 'TKeypress'
    KB_ALT_SHIFT_N: 'TKeypress'
    KB_ALT_SHIFT_O: 'TKeypress'
    KB_ALT_SHIFT_P: 'TKeypress'
    KB_ALT_SHIFT_Q: 'TKeypress'
    KB_ALT_SHIFT_R: 'TKeypress'
    KB_ALT_SHIFT_S: 'TKeypress'
    KB_ALT_SHIFT_T: 'TKeypress'
    KB_ALT_SHIFT_U: 'TKeypress'
    KB_ALT_SHIFT_V: 'TKeypress'
    KB_ALT_SHIFT_W: 'TKeypress'
    KB_ALT_SHIFT_X: 'TKeypress'
    KB_ALT_SHIFT_Y: 'TKeypress'
    KB_ALT_SHIFT_Z: 'TKeypress'

    KB_ALT_SHIFT_HOME: 'TKeypress'
    KB_ALT_SHIFT_END: 'TKeypress'
    KB_ALT_SHIFT_PG_UP: 'TKeypress'
    KB_ALT_SHIFT_PG_DN: 'TKeypress'
    KB_ALT_SHIFT_UP: 'TKeypress'
    KB_ALT_SHIFT_DOWN: 'TKeypress'
    KB_ALT_SHIFT_LEFT: 'TKeypress'
    KB_ALT_SHIFT_RIGHT: 'TKeypress'

    KB_CTRL_SHIFT_HOME: 'TKeypress'
    KB_CTRL_SHIFT_END: 'TKeypress'
    KB_CTRL_SHIFT_PG_UP: 'TKeypress'
    KB_CTRL_SHIFT_PG_DN: 'TKeypress'
    KB_CTRL_SHIFT_UP: 'TKeypress'
    KB_CTRL_SHIFT_DOWN: 'TKeypress'
    KB_CTRL_SHIFT_LEFT: 'TKeypress'
    KB_CTRL_SHIFT_RIGHT: 'TKeypress'
    KB_BACKSPACE: 'TKeypress'
    KB_CTRL_BACKSPACE: 'TKeypress'
    KB_ALT_BACKSPACE: 'TKeypress'
    KB_BACKSPACE_DEL: 'TKeypress'


# Special "no-key" keypress, used to ignore undefined keystrokes
TKeypress.KB_NO_KEY = TKeypress(True, TKeypress.NONE, ord(' '), False, False, False)

# Normal keys
TKeypress.KB_F1 = TKeypress(True, TKeypress.F1, ord(' '), False, False, False)
TKeypress.KB_F2 = TKeypress(True, TKeypress.F2, ord(' '), False, False, False)
TKeypress.KB_F3 = TKeypress(True, TKeypress.F3, ord(' '), False, False, False)
TKeypress.KB_F4 = TKeypress(True, TKeypress.F4, ord(' '), False, False, False)
TKeypress.KB_F5 = TKeypress(True, TKeypress.F5, ord(' '), False, False, False)
TKeypress.KB_F6 = TKeypress(True, TKeypress.F6, ord(' '), False, False, False)
TKeypress.KB_F7 = TKeypress(True, TKeypress.F7, ord(' '), False, False, False)
TKeypress.KB_F8 = TKeypress(True, TKeypress.F8, ord(' '), False, False, False)
TKeypress.KB_F9 = TKeypress(True, TKeypress.F9, ord(' '), False, False, False)
TKeypress.KB_F10 = TKeypress(True, TKeypress.F10, ord(' '), False, False, False)
TKeypress.KB_F11 = TKeypress(True, TKeypress.F11, ord(' '), False, False, False)
TKeypress.KB_F12 = TKeypress(True, TKeypress.F12, ord(' '), False, False, False)
TKeypress.KB_ALT_F1 = TKeypress(True, TKeypress.F1, ord(' '), True, False, False)
TKeypress.KB_ALT_F2 = TKeypress(True, TKeypress.F2, ord(' '), True, False, False)
TKeypress.KB_ALT_F3 = TKeypress(True, TKeypress.F3, ord(' '), True, False, False)
TKeypress.KB_ALT_F4 = TKeypress(True, TKeypress.F4, ord(' '), True, False, False)
TKeypress.KB_ALT_F5 = TKeypress(True, TKeypress.F5, ord(' '), True, False, False)
TKeypress.KB_ALT_F6 = TKeypress(True, TKeypress.F6, ord(' '), True, False, False)
TKeypress.KB_ALT_F7 = TKeypress(True, TKeypress.F7, ord(' '), True, False, False)
TKeypress.KB_ALT_F8 = TKeypress(True, TKeypress.F8, ord(' '), True, False, False)
TKeypress.KB_ALT_F9 = TKeypress(True, TKeypress.F9, ord(' '), True, False, False)
TKeypress.KB_ALT_F10 = TKeypress(True, TKeypress.F10, ord(' '), True, False, False)
TKeypress.KB_ALT_F11 = TKeypress(True, TKeypress.F11, ord(' '), True, False, False)
TKeypress.KB_ALT_F12 = TKeypress(True, TKeypress.F12, ord(' '), True, False, False)
TKeypress.KB_CTRL_F1 = TKeypress(True, TKeypress.F1, ord(' '), False, True, False)
TKeypress.KB_CTRL_F2 = TKeypress(True, TKeypress.F2, ord(' '), False, True, False)
TKeypress.KB_CTRL_F3 = TKeypress(True, TKeypress.F3, ord(' '), False, True, False)
TKeypress.KB_CTRL_F4 = TKeypress(True, TKeypress.F4, ord(' '), False, True, False)
TKeypress.KB_CTRL_F5 = TKeypress(True, TKeypress.F5, ord(' '), False, True, False)
TKeypress.KB_CTRL_F6 = TKeypress(True, TKeypress.F6, ord(' '), False, True, False)
TKeypress.KB_CTRL_F7 = TKeypress(True, TKeypress.F7, ord(' '), False, True, False)
TKeypress.KB_CTRL_F8 = TKeypress(True, TKeypress.F8, ord(' '), False, True, False)
TKeypress.KB_CTRL_F9 = TKeypress(True, TKeypress.F9, ord(' '), False, True, False)
TKeypress.KB_CTRL_F10 = TKeypress(True, TKeypress.F10, ord(' '), False, True, False)
TKeypress.KB_CTRL_F11 = TKeypress(True, TKeypress.F11, ord(' '), False, True, False)
TKeypress.KB_CTRL_F12 = TKeypress(True, TKeypress.F12, ord(' '), False, True, False)
TKeypress.KB_SHIFT_F1 = TKeypress(True, TKeypress.F1, ord(' '), False, False, True)
TKeypress.KB_SHIFT_F2 = TKeypress(True, TKeypress.F2, ord(' '), False, False, True)
TKeypress.KB_SHIFT_F3 = TKeypress(True, TKeypress.F3, ord(' '), False, False, True)
TKeypress.KB_SHIFT_F4 = TKeypress(True, TKeypress.F4, ord(' '), False, False, True)
TKeypress.KB_SHIFT_F5 = TKeypress(True, TKeypress.F5, ord(' '), False, False, True)
TKeypress.KB_SHIFT_F6 = TKeypress(True, TKeypress.F6, ord(' '), False, False, True)
TKeypress.KB_SHIFT_F7 = TKeypress(True, TKeypress.F7, ord(' '), False, False, True)
TKeypress.KB_SHIFT_F8 = TKeypress(True, TKeypress.F8, ord(' '), False, False, True)
TKeypress.KB_SHIFT_F9 = TKeypress(True, TKeypress.F9, ord(' '), False, False, True)
TKeypress.KB_SHIFT_F10 = TKeypress(True, TKeypress.F10, ord(' '), False, False, True)
TKeypress.KB_SHIFT_F11 = TKeypress(True, TKeypress.F11, ord(' '), False, False, True)
TKeypress.KB_SHIFT_F12 = TKeypress(True, TKeypress.F12, ord(' '), False, False, True)
TKeypress.KB_ENTER = TKeypress(True, TKeypress.ENTER, ord(' '), False, False, False)
TKeypress.KB_TAB = TKeypress(True, TKeypress.TAB, ord(' '), False, False, False)
TKeypress.KB_ESC = TKeypress(True, TKeypress.ESC, ord(' '), False, False, False)
TKeypress.KB_HOME = TKeypress(True, TKeypress.HOME, ord(' '), False, False, False)
TKeypress.KB_END = TKeypress(True, TKeypress.END, ord(' '), False, False, False)
TKeypress.KB_PG_UP = TKeypress(True, TKeypress.PGUP, ord(' '), False, False, False)
TKeypress.KB_PG_DN = TKeypress(True, TKeypress.PGDN, ord(' '), False, False, False)
TKeypress.KB_INS = TKeypress(True, TKeypress.INS, ord(' '), False, False, False)
TKeypress.KB_DEL = TKeypress(True, TKeypress.DEL, ord(' '), False, False, False)
TKeypress.KB_UP = TKeypress(True, TKeypress.UP, ord(' '), False, False, False)
TKeypress.KB_DOWN = TKeypress(True, TKeypress.DOWN, ord(' '), False, False, False)
TKeypress.KB_LEFT = TKeypress(True, TKeypress.LEFT, ord(' '), False, False, False)
TKeypress.KB_RIGHT = TKeypress(True, TKeypress.RIGHT, ord(' '), False, False, False)
TKeypress.KB_ALT_ENTER = TKeypress(True, TKeypress.ENTER, ord(' '), True, False, False)
TKeypress.KB_ALT_TAB = TKeypress(True, TKeypress.TAB, ord(' '), True, False, False)
TKeypress.KB_ALT_ESC = TKeypress(True, TKeypress.ESC, ord(' '), True, False, False)
TKeypress.KB_ALT_HOME = TKeypress(True, TKeypress.HOME, ord(' '), True, False, False)
TKeypress.KB_ALT_END = TKeypress(True, TKeypress.END, ord(' '), True, False, False)
TKeypress.KB_ALT_PG_UP = TKeypress(True, TKeypress.PGUP, ord(' '), True, False, False)
TKeypress.KB_ALT_PG_DN = TKeypress(True, TKeypress.PGDN, ord(' '), True, False, False)
TKeypress.KB_ALT_INS = TKeypress(True, TKeypress.INS, ord(' '), True, False, False)
TKeypress.KB_ALT_DEL = TKeypress(True, TKeypress.DEL, ord(' '), True, False, False)
TKeypress.KB_ALT_UP = TKeypress(True, TKeypress.UP, ord(' '), True, False, False)
TKeypress.KB_ALT_DOWN = TKeypress(True, TKeypress.DOWN, ord(' '), True, False, False)
TKeypress.KB_ALT_LEFT = TKeypress(True, TKeypress.LEFT, ord(' '), True, False, False)
TKeypress.KB_ALT_RIGHT = TKeypress(True, TKeypress.RIGHT, ord(' '), True, False, False)
TKeypress.KB_CTRL_ENTER = TKeypress(True, TKeypress.ENTER, ord(' '), False, True, False)
TKeypress.KB_CTRL_TAB = TKeypress(True, TKeypress.TAB, ord(' '), False, True, False)
TKeypress.KB_CTRL_ESC = TKeypress(True, TKeypress.ESC, ord(' '), False, True, False)
TKeypress.KB_CTRL_HOME = TKeypress(True, TKeypress.HOME, ord(' '), False, True, False)
TKeypress.KB_CTRL_END = TKeypress(True, TKeypress.END, ord(' '), False, True, False)
TKeypress.KB_CTRL_PG_UP = TKeypress(True, TKeypress.PGUP, ord(' '), False, True, False)
TKeypress.KB_CTRL_PG_DN = TKeypress(True, TKeypress.PGDN, ord(' '), False, True, False)
TKeypress.KB_CTRL_INS = TKeypress(True, TKeypress.INS, ord(' '), False, True, False)
TKeypress.KB_CTRL_DEL = TKeypress(True, TKeypress.DEL, ord(' '), False, True, False)
TKeypress.KB_CTRL_UP = TKeypress(True, TKeypress.UP, ord(' '), False, True, False)
TKeypress.KB_CTRL_DOWN = TKeypress(True, TKeypress.DOWN, ord(' '), False, True, False)
TKeypress.KB_CTRL_LEFT = TKeypress(True, TKeypress.LEFT, ord(' '), False, True, False)
TKeypress.KB_CTRL_RIGHT = TKeypress(True, TKeypress.RIGHT, ord(' '), False, True, False)
TKeypress.KB_SHIFT_ENTER = TKeypress(True, TKeypress.ENTER, ord(' '), False, False, True)
TKeypress.KB_SHIFT_TAB = TKeypress(True, TKeypress.TAB, ord(' '), False, False, True)
TKeypress.KB_BACK_TAB = TKeypress(True, TKeypress.BTAB, ord(' '), False, False, False)
TKeypress.KB_SHIFT_ESC = TKeypress(True, TKeypress.ESC, ord(' '), False, False, True)
TKeypress.KB_SHIFT_HOME = TKeypress(True, TKeypress.HOME, ord(' '), False, False, True)
TKeypress.KB_SHIFT_END = TKeypress(True, TKeypress.END, ord(' '), False, False, True)
TKeypress.KB_SHIFT_PG_UP = TKeypress(True, TKeypress.PGUP, ord(' '), False, False, True)
TKeypress.KB_SHIFT_PG_DN = TKeypress(True, TKeypress.PGDN, ord(' '), False, False, True)
TKeypress.KB_SHIFT_INS = TKeypress(True, TKeypress.INS, ord(' '), False, False, True)
TKeypress.KB_SHIFT_DEL = TKeypress(True, TKeypress.DEL, ord(' '), False, False, True)
TKeypress.KB_SHIFT_UP = TKeypress(True, TKeypress.UP, ord(' '), False, False, True)
TKeypress.KB_SHIFT_DOWN = TKeypress(True, TKeypress.DOWN, ord(' '), False, False, True)
TKeypress.KB_SHIFT_LEFT = TKeypress(True, TKeypress.LEFT, ord(' '), False, False, True)
TKeypress.KB_SHIFT_RIGHT = TKeypress(True, TKeypress.RIGHT, ord(' '), False, False, True)
TKeypress.KB_A = TKeypress(False, 0, ord('a'), False, False, False)
TKeypress.KB_B = TKeypress(False, 0, ord('b'), False, False, False)
TKeypress.KB_C = TKeypress(False, 0, ord('c'), False, False, False)
TKeypress.KB_D = TKeypress(False, 0, ord('d'), False, False, False)
TKeypress.KB_E = TKeypress(False, 0, ord('e'), False, False, False)
TKeypress.KB_F = TKeypress(False, 0, ord('f'), False, False, False)
TKeypress.KB_G = TKeypress(False, 0, ord('g'), False, False, False)
TKeypress.KB_H = TKeypress(False, 0, ord('h'), False, False, False)
TKeypress.KB_I = TKeypress(False, 0, ord('i'), False, False, False)
TKeypress.KB_J = TKeypress(False, 0, ord('j'), False, False, False)
TKeypress.KB_K = TKeypress(False, 0, ord('k'), False, False, False)
TKeypress.KB_L = TKeypress(False, 0, ord('l'), False, False, False)
TKeypress.KB_M = TKeypress(False, 0, ord('m'), False, False, False)
TKeypress.KB_N = TKeypress(False, 0, ord('n'), False, False, False)
TKeypress.KB_O = TKeypress(False, 0, ord('o'), False, False, False)
TKeypress.KB_P = TKeypress(False, 0, ord('p'), False, False, False)
TKeypress.KB_Q = TKeypress(False, 0, ord('q'), False, False, False)
TKeypress.KB_R = TKeypress(False, 0, ord('r'), False, False, False)
TKeypress.KB_S = TKeypress(False, 0, ord('s'), False, False, False)
TKeypress.KB_T = TKeypress(False, 0, ord('t'), False, False, False)
TKeypress.KB_U = TKeypress(False, 0, ord('u'), False, False, False)
TKeypress.KB_V = TKeypress(False, 0, ord('v'), False, False, False)
TKeypress.KB_W = TKeypress(False, 0, ord('w'), False, False, False)
TKeypress.KB_X = TKeypress(False, 0, ord('x'), False, False, False)
TKeypress.KB_Y = TKeypress(False, 0, ord('y'), False, False, False)
TKeypress.KB_Z = TKeypress(False, 0, ord('z'), False, False, False)
TKeypress.KB_SPACE = TKeypress(False, 0, ord(' '), False, False, False)
TKeypress.KB_ALT_A = TKeypress(False, 0, ord('a'), True, False, False)
TKeypress.KB_ALT_B = TKeypress(False, 0, ord('b'), True, False, False)
TKeypress.KB_ALT_C = TKeypress(False, 0, ord('c'), True, False, False)
TKeypress.KB_ALT_D = TKeypress(False, 0, ord('d'), True, False, False)
TKeypress.KB_ALT_E = TKeypress(False, 0, ord('e'), True, False, False)
TKeypress.KB_ALT_F = TKeypress(False, 0, ord('f'), True, False, False)
TKeypress.KB_ALT_G = TKeypress(False, 0, ord('g'), True, False, False)
TKeypress.KB_ALT_H = TKeypress(False, 0, ord('h'), True, False, False)
TKeypress.KB_ALT_I = TKeypress(False, 0, ord('i'), True, False, False)
TKeypress.KB_ALT_J = TKeypress(False, 0, ord('j'), True, False, False)
TKeypress.KB_ALT_K = TKeypress(False, 0, ord('k'), True, False, False)
TKeypress.KB_ALT_L = TKeypress(False, 0, ord('l'), True, False, False)
TKeypress.KB_ALT_M = TKeypress(False, 0, ord('m'), True, False, False)
TKeypress.KB_ALT_N = TKeypress(False, 0, ord('n'), True, False, False)
TKeypress.KB_ALT_O = TKeypress(False, 0, ord('o'), True, False, False)
TKeypress.KB_ALT_P = TKeypress(False, 0, ord('p'), True, False, False)
TKeypress.KB_ALT_Q = TKeypress(False, 0, ord('q'), True, False, False)
TKeypress.KB_ALT_R = TKeypress(False, 0, ord('r'), True, False, False)
TKeypress.KB_ALT_S = TKeypress(False, 0, ord('s'), True, False, False)
TKeypress.KB_ALT_T = TKeypress(False, 0, ord('t'), True, False, False)
TKeypress.KB_ALT_U = TKeypress(False, 0, ord('u'), True, False, False)
TKeypress.KB_ALT_V = TKeypress(False, 0, ord('v'), True, False, False)
TKeypress.KB_ALT_W = TKeypress(False, 0, ord('w'), True, False, False)
TKeypress.KB_ALT_X = TKeypress(False, 0, ord('x'), True, False, False)
TKeypress.KB_ALT_Y = TKeypress(False, 0, ord('y'), True, False, False)
TKeypress.KB_ALT_Z = TKeypress(False, 0, ord('z'), True, False, False)
TKeypress.KB_ALT0 = TKeypress(False, 0, ord('0'), True, False, False)
TKeypress.KB_ALT1 = TKeypress(False, 0, ord('1'), True, False, False)
TKeypress.KB_ALT2 = TKeypress(False, 0, ord('2'), True, False, False)
TKeypress.KB_ALT3 = TKeypress(False, 0, ord('3'), True, False, False)
TKeypress.KB_ALT4 = TKeypress(False, 0, ord('4'), True, False, False)
TKeypress.KB_ALT5 = TKeypress(False, 0, ord('5'), True, False, False)
TKeypress.KB_ALT6 = TKeypress(False, 0, ord('6'), True, False, False)
TKeypress.KB_ALT7 = TKeypress(False, 0, ord('7'), True, False, False)
TKeypress.KB_ALT8 = TKeypress(False, 0, ord('8'), True, False, False)
TKeypress.KB_ALT9 = TKeypress(False, 0, ord('9'), True, False, False)
TKeypress.KB_CTRL_A = TKeypress(False, 0, ord('A'), False, True, False)
TKeypress.KB_CTRL_B = TKeypress(False, 0, ord('B'), False, True, False)
TKeypress.KB_CTRL_C = TKeypress(False, 0, ord('C'), False, True, False)
TKeypress.KB_CTRL_D = TKeypress(False, 0, ord('D'), False, True, False)
TKeypress.KB_CTRL_E = TKeypress(False, 0, ord('E'), False, True, False)
TKeypress.KB_CTRL_F = TKeypress(False, 0, ord('F'), False, True, False)
TKeypress.KB_CTRL_G = TKeypress(False, 0, ord('G'), False, True, False)
TKeypress.KB_CTRL_H = TKeypress(False, 0, ord('H'), False, True, False)
TKeypress.KB_CTRL_I = TKeypress(False, 0, ord('I'), False, True, False)
TKeypress.KB_CTRL_J = TKeypress(False, 0, ord('J'), False, True, False)
TKeypress.KB_CTRL_K = TKeypress(False, 0, ord('K'), False, True, False)
TKeypress.KB_CTRL_L = TKeypress(False, 0, ord('L'), False, True, False)
TKeypress.KB_CTRL_M = TKeypress(False, 0, ord('M'), False, True, False)
TKeypress.KB_CTRL_N = TKeypress(False, 0, ord('N'), False, True, False)
TKeypress.KB_CTRL_O = TKeypress(False, 0, ord('O'), False, True, False)
TKeypress.KB_CTRL_P = TKeypress(False, 0, ord('P'), False, True, False)
TKeypress.KB_CTRL_Q = TKeypress(False, 0, ord('Q'), False, True, False)
TKeypress.KB_CTRL_R = TKeypress(False, 0, ord('R'), False, True, False)
TKeypress.KB_CTRL_S = TKeypress(False, 0, ord('S'), False, True, False)
TKeypress.KB_CTRL_T = TKeypress(False, 0, ord('T'), False, True, False)
TKeypress.KB_CTRL_U = TKeypress(False, 0, ord('U'), False, True, False)
TKeypress.KB_CTRL_V = TKeypress(False, 0, ord('V'), False, True, False)
TKeypress.KB_CTRL_W = TKeypress(False, 0, ord('W'), False, True, False)
TKeypress.KB_CTRL_X = TKeypress(False, 0, ord('X'), False, True, False)
TKeypress.KB_CTRL_Y = TKeypress(False, 0, ord('Y'), False, True, False)
TKeypress.KB_CTRL_Z = TKeypress(False, 0, ord('Z'), False, True, False)
TKeypress.KB_ALT_SHIFT_A = TKeypress(False, 0, ord('A'), True, False, True)
TKeypress.KB_ALT_SHIFT_B = TKeypress(False, 0, ord('B'), True, False, True)
TKeypress.KB_ALT_SHIFT_C = TKeypress(False, 0, ord('C'), True, False, True)
TKeypress.KB_ALT_SHIFT_D = TKeypress(False, 0, ord('D'), True, False, True)
TKeypress.KB_ALT_SHIFT_E = TKeypress(False, 0, ord('E'), True, False, True)
TKeypress.KB_ALT_SHIFT_F = TKeypress(False, 0, ord('F'), True, False, True)
TKeypress.KB_ALT_SHIFT_G = TKeypress(False, 0, ord('G'), True, False, True)
TKeypress.KB_ALT_SHIFT_H = TKeypress(False, 0, ord('H'), True, False, True)
TKeypress.KB_ALT_SHIFT_I = TKeypress(False, 0, ord('I'), True, False, True)
TKeypress.KB_ALT_SHIFT_J = TKeypress(False, 0, ord('J'), True, False, True)
TKeypress.KB_ALT_SHIFT_K = TKeypress(False, 0, ord('K'), True, False, True)
TKeypress.KB_ALT_SHIFT_L = TKeypress(False, 0, ord('L'), True, False, True)
TKeypress.KB_ALT_SHIFT_M = TKeypress(False, 0, ord('M'), True, False, True)
TKeypress.KB_ALT_SHIFT_N = TKeypress(False, 0, ord('N'), True, False, True)
TKeypress.KB_ALT_SHIFT_O = TKeypress(False, 0, ord('O'), True, False, True)
TKeypress.KB_ALT_SHIFT_P = TKeypress(False, 0, ord('P'), True, False, True)
TKeypress.KB_ALT_SHIFT_Q = TKeypress(False, 0, ord('Q'), True, False, True)
TKeypress.KB_ALT_SHIFT_R = TKeypress(False, 0, ord('R'), True, False, True)
TKeypress.KB_ALT_SHIFT_S = TKeypress(False, 0, ord('S'), True, False, True)
TKeypress.KB_ALT_SHIFT_T = TKeypress(False, 0, ord('T'), True, False, True)
TKeypress.KB_ALT_SHIFT_U = TKeypress(False, 0, ord('U'), True, False, True)
TKeypress.KB_ALT_SHIFT_V = TKeypress(False, 0, ord('V'), True, False, True)
TKeypress.KB_ALT_SHIFT_W = TKeypress(False, 0, ord('W'), True, False, True)
TKeypress.KB_ALT_SHIFT_X = TKeypress(False, 0, ord('X'), True, False, True)
TKeypress.KB_ALT_SHIFT_Y = TKeypress(False, 0, ord('Y'), True, False, True)
TKeypress.KB_ALT_SHIFT_Z = TKeypress(False, 0, ord('Z'), True, False, True)

TKeypress.KB_ALT_SHIFT_HOME = TKeypress(True, TKeypress.HOME, ord(' '), True, False, True)
TKeypress.KB_ALT_SHIFT_END = TKeypress(True, TKeypress.END, ord(' '), True, False, True)
TKeypress.KB_ALT_SHIFT_PG_UP = TKeypress(True, TKeypress.PGUP, ord(' '), True, False, True)
TKeypress.KB_ALT_SHIFT_PG_DN = TKeypress(True, TKeypress.PGDN, ord(' '), True, False, True)
TKeypress.KB_ALT_SHIFT_UP = TKeypress(True, TKeypress.UP, ord(' '), True, False, True)
TKeypress.KB_ALT_SHIFT_DOWN = TKeypress(True, TKeypress.DOWN, ord(' '), True, False, True)
TKeypress.KB_ALT_SHIFT_LEFT = TKeypress(True, TKeypress.LEFT, ord(' '), True, False, True)
TKeypress.KB_ALT_SHIFT_RIGHT = TKeypress(True, TKeypress.RIGHT, ord(' '), True, False, True)

TKeypress.KB_CTRL_SHIFT_HOME = TKeypress(True, TKeypress.HOME, ord(' '), False, True, True)
TKeypress.KB_CTRL_SHIFT_END = TKeypress(True, TKeypress.END, ord(' '), False, True, True)
TKeypress.KB_CTRL_SHIFT_PG_UP = TKeypress(True, TKeypress.PGUP, ord(' '), False, True, True)
TKeypress.KB_CTRL_SHIFT_PG_DN = TKeypress(True, TKeypress.PGDN, ord(' '), False, True, True)
TKeypress.KB_CTRL_SHIFT_UP = TKeypress(True, TKeypress.UP, ord(' '), False, True, True)
TKeypress.KB_CTRL_SHIFT_DOWN = TKeypress(True, TKeypress.DOWN, ord(' '), False, True, True)
TKeypress.KB_CTRL_SHIFT_LEFT = TKeypress(True, TKeypress.LEFT, ord(' '), False, True, True)
TKeypress.KB_CTRL_SHIFT_RIGHT = TKeypress(True, TKeypress.RIGHT, ord(' '), False, True, True)

# Backspace as ^H
TKeypress.KB_BACKSPACE = TKeypress(False, 0, ord('H'), False, True, False)

# Control-Backspace as function key.
TKeypress.KB_CTRL_BACKSPACE = TKeypress(True, TKeypress.BACKSPACE, ord(' '), False, True, False)

# Alt-Backspace as function key.
TKeypress.KB_ALT_BACKSPACE = TKeypress(True, TKeypress.BACKSPACE, ord(' '), True, False, False)

# Backspace as ^?.
TKeypress.KB_BACKSPACE_DEL = TKeypress(False, 0, 0x7F, False, False, False)
