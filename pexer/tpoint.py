from typing import NamedTuple

__all__ = ['TPoint']


class TPoint(NamedTuple):
    """
    A TPoint represents a location on the screen in terms of an horizontal x co-ordinate and
    vertical y co-ordinate.

    """
    x: int
    y: int
