import logging
from typing import Optional, List, Tuple, cast

from .backend import LogicalScreen
from .bits import ColorTheme
from .event import TKeypressEvent, TMouseEvent, TInputEvent, TCommandEvent, TResizeEvent
from .size import TOptionalSize, TSize
from .tpoint import TPoint

# When type-checking we need some classes which would be a circular import if imported directly.
import pexer

__all__ = ['TWidget', 'TPoint']

LOG = logging.getLogger(__name__)


class TWidget:
    """
    TWidget is the base class from which all on-screen widgets are derived.

    The initial size of the widget can be specified via *size* and the top-left corner via
    *poisition*. The *align*, *grow* and *shrink* arguments are used to indicate packing behavior
    in layout widgets. Note that *size* and *position* may not always be respected if the widget is
    a child of a layout container.

    Widgets can have zero or more child widgets which are drawn in order of the children list.
    Override the draw() method to insert one's own drawing code before or after this.

    """
    def __init__(
            self, *,
            parent: Optional['TWidget'] = None,
            position: TPoint = TPoint(0, 0),
            size: TSize = TSize(),
            grow: float = 0., shrink: float = 0.):
        self._application: Optional['pexer.TApplication'] = None
        self._parent = parent
        self._position = position
        self._size = size
        self._shrink = float(shrink)
        self._grow = float(grow)
        self._children: List['TWidget'] = []

        # The child of _this_ widget which would be active if this widget were also active.
        self._active_child: Optional[TWidget] = None

    @property
    def parent(self) -> Optional['TWidget']:
        return self._parent

    def set_parent(self, parent: Optional['TWidget']) -> None:
        self._parent = parent

    @property
    def size(self) -> TSize:
        return self._size

    def set_size(self, size: TSize) -> None:
        """
        Set the size of this widget. Subclasses can override this method to be notified of
        resizing.

        """
        self._size = size

    @property
    def position(self) -> TPoint:
        return self._position

    def set_position(self, position: TPoint) -> None:
        """
        Reposition this widget. Subclasses can override this method to handle reposition. The
        superclass method must be called to record the new position.

        """
        self._position = position

    @property
    def grow(self) -> float:
        return self._grow

    def set_grow(self, grow: float) -> None:
        self._grow = grow

    @property
    def shrink(self) -> float:
        return self._shrink

    def set_shrink(self, shrink: float) -> None:
        self._shrink = shrink

    @property
    def direct_application(self) -> Optional['pexer.TApplication']:
        """
        Any application which has been directly set on the widget. Usually you want to use the
        get_application() method rather than this property.

        """
        return self._application

    def set_direct_application(self, application: Optional['pexer.TApplication']) -> None:
        """
        Set (or clear) the application directly associated with this widget. Rarely used outside of
        TApplication's implementation.

        """
        self._application = application

    @property
    def application(self) -> Optional['pexer.TApplication']:
        """
        Walk the widget heirarchy to find the ultimate application for this widget. Returns None if
        there is no application.

        """
        if self.direct_application is not None:
            return self.direct_application
        if self.parent is not None:
            return self.parent.application
        return None

    @property
    def theme(self) -> ColorTheme:
        app = self.application
        assert app is not None
        return app.theme

    @property
    def children(self) -> Tuple['TWidget']:
        """
        Current children as an immutable tuple. To modify the list of children, use the pack(),
        pack_at() and unpack() methods.

        """
        return cast(Tuple[TWidget], tuple(self._children))

    @property
    def active_child(self) -> Optional['TWidget']:
        return self._active_child

    def set_active_child(self, child: Optional['TWidget']):
        if self._active_child is not None:
            self._active_child.on_deactivate()
        if child is not None and child not in self._children:
            raise ValueError("Passed child widget is not a child of this TWidget.")
        self._active_child = child
        if self._active_child is not None:
            self._active_child.on_activate()

    @property
    def is_active(self) -> bool:
        """
        A flag indicating if this widget is the active child of its parent _and_ if its parent is
        active.

        If the widget has no parent, it is always active.

        """
        return (self.parent is None) or (
            self.parent.active_child is self
            and self.parent.is_active
        )

    def pack(self, child: 'TWidget') -> None:
        """
        Pack a new child into this widget at the end of the list of children. Raises ValueError if
        *child* is already a child.

        """
        if child in self._children:
            raise ValueError("Child already present in TWidget.")
        child.set_parent(self)
        self._children.append(child)
        self.on_children_changed()

    def pack_at(self, child: 'TWidget', index: int) -> None:
        """
        Pack a new child at index *index*. Negative indices are interpreted as per Python's usual
        convention. Raises ValueError if *child* is already a child. Raises *IndexError* if the
        index is out of range.

        """
        if child in self._children:
            raise ValueError("Child already present in TWidget.")
        child.set_parent(self)
        self._children.insert(index, child)
        self.on_children_changed()

    def unpack(self, child: 'TWidget') -> None:
        """
        Remove a chuld from this widget. Raises ValueError if *child* is not a child of the
        widget.

        """
        if child is self._active_child:
            self.set_active_child(None)
        self._children.remove(child)
        child.set_parent(None)
        self.on_children_changed()

    def unpack_all(self) -> None:
        """
        Remove all chuldren from this widget.

        """
        if self._active_child is not None:
            self.set_active_child(None)
        for child in self._children:
            child.set_parent(None)
        self._children = []
        self.on_children_changed()

    def activate(self) -> None:
        """
        Activate this widget within its parent and attempt to make its parent the active widget.

        """
        if self.parent is None:
            return
        self.parent.set_active_child(self)
        self.parent.activate()

    def deactivate(self) -> None:
        """
        Dectivate this widget within its parent.

        """
        if self.parent is None or self.parent.active_child is not self:
            return
        self.parent.set_active_child(None)

    def activate_next_child(self) -> None:
        """
        Activate the next child whose turn it is to be active.

        """
        if self._active_child is None or len(self._children) == 0:
            return
        idx = self._children.index(self._active_child)
        if idx < 0:
            return
        self.set_active_child(self._children[(idx + 1) % len(self._children)])

    def activate_previous_child(self) -> None:
        """
        Activate the previous child whose turn it is to be active.

        """
        if self._active_child is None or len(self._children) == 0:
            return
        idx = self._children.index(self._active_child)
        if idx < 0:
            return
        self.set_active_child(
            self._children[(idx - 1 + len(self._children)) % len(self._children)])

    def preferred_size(self, hint: TOptionalSize) -> TOptionalSize:
        """
        Return the *preferred* size of this widget given the passed size hint. Either the return
        value or the hint value can indicate "don't care" via None.

        A widget which can take any size simply returns *hint*.

        The default implementation returns *hint*.

        """
        return hint

    def minimum_size(self, hint: TOptionalSize) -> TOptionalSize:
        """
        Return the *minimum* size of the widget given the passed size hint. Either the return value
        or the hint value can indicate "don't care" via None.

        The default implementation returns "don't care".

        """
        return TOptionalSize()

    def maximum_size(self, hint: TOptionalSize) -> TOptionalSize:
        """
        Return the *maximum* size of the widget given the passed size hint. Either the return value
        or the hint value can indicate "don't care" via None.

        The default implementation returns "don't care".

        """
        return TOptionalSize()

    def draw(self, screen: LogicalScreen) -> None:
        """
        Draw this widget on the passed screen. The screen's clipping rectangle will have been set
        to the widget's bounds and the offset set such the the screen't logical (0, 0) co-ordinate
        represents the top-left of the widget.

        Note that a scrolled widget might not have the (0, 0) area visible.

        """
        for child in self.children:
            w, h = child.size
            x, y = child.position

            with screen.saving_context():
                screen.translate(x, y)
                screen.clip_to(0, 0, w, h)
                child.draw(screen)

    def on_activate(self) -> None:
        """
        Called if this widget is activated. Default behaviour is to pass this to the active child
        while recording activated state in the is_active attribute.

        """
        if self.active_child is not None:
            self.active_child.on_activate()

    def on_deactivate(self) -> None:
        """
        Called if this widget is deactivated. Default behaviour is to pass this to the active
        child while recording deactivated state in the is_active attribute.

        """
        if self.active_child is not None:
            self.active_child.on_deactivate()

    def on_children_changed(self) -> None:
        """
        Called if the list of children has changed. Default behaviour is do nothing but subclasses
        should still make sure this is called for future compatibility.

        """

    def on_input_event(self, event: TInputEvent) -> bool:
        """
        Process a passed input event. If this widget or one of its subclasses handled the event,
        return True otherwise return False.

        Usually subclasses should not need to override this method and should instead override one
        of the on_...() methods instead.

        """
        if isinstance(event, TKeypressEvent):
            return self.on_keypress(event)
        elif isinstance(event, TMouseEvent):
            return self.on_mouse(event)
        elif isinstance(event, TCommandEvent):
            return self.on_command(event)
        elif isinstance(event, TResizeEvent) and event.type == TResizeEvent.Type.WIDGET:
            return self.on_resize(event)

        # Unknown event type
        return False

    def on_keypress(self, event: TKeypressEvent) -> bool:
        """
        Handle a keypress event. Return True if the event was consumed by theis widget or a child
        of this widget.

        """
        # TODO: active child
        return False

    def on_mouse(self, event: TMouseEvent) -> bool:
        """
        Handle a mouse event. Return True if the event was consumed by theis widget or a child
        of this widget. The incoming event's x and y attributes are relative to the widget's
        top-left. Absolute screen positions can be determined via the absolute_x and absolute_y
        attribures.

        Default behavior is to find the top-most child whose bounds intersect the mouse event and
        pass it on. The child must handle the event to stop children underneath it also receiving
        the event.

        """
        ex, ey = event.x, event.y
        for child in self.children[::-1]:
            w, h = child.size
            x, y = child.position
            if ex >= x and ex < x + w and ey >= y and ey < y + h:
                event.x = ex - x
                event.y = ey - y
                handled = child.on_mouse(event)
                event.x = ex
                event.y = ey
                if handled:
                    return True

        return False

    def on_command(self, event: TCommandEvent) -> bool:
        """
        Handle a command event. Return True if the event was consumed by theis widget or a child
        of this widget.

        """
        # TODO: active child
        return False

    def on_resize(self, event: TResizeEvent) -> bool:
        """
        Handle a widget resize event. Return True if the event was consumed by this widget or a
        child of this widget. The default implementation calls self.set_size().

        """
        self._size = TSize(event.width, event.height)
        return True
