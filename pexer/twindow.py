from typing import Any, Optional

from .backend import LogicalScreen
from .bits import stringutils
from .event import TMouseEvent
from .size import TSize
from .tfill import TFill
from .tpoint import TPoint
from .twidget import TWidget

__all__ = ['TWindow', 'TWindowStack']


class TWindow(TWidget):
    """
    A TWindow contains a single widget within a resizable frame.

    """
    def __init__(
            self, *,
            title: Optional[str] = None,
            content: Optional[TWidget] = None,
            **kwargs: Any) -> None:
        super().__init__(**kwargs)
        self._title = title
        self.set_content(content)

    @property
    def title(self) -> Optional[str]:
        return self._title

    def set_title(self, title: Optional[str]) -> None:
        """
        Set the window title.

        """
        self._title = title

    @property
    def content(self) -> Optional[TWidget]:
        try:
            return self.children[0]
        except IndexError:
            return None

    def set_content(self, content: Optional[TWidget]) -> None:
        self.unpack_all()
        if content is not None:
            self.pack(content)

    def set_size(self, size: TSize) -> None:
        super().set_size(size)
        self._recompute_layout()

    def on_children_changed(self) -> None:
        self._recompute_layout()

    def _recompute_layout(self) -> None:
        w, h = self.size
        for child in self.children:
            child.set_position(TPoint(1, 1))
            child.set_size(TSize(max(0, w-2), max(0, h-2)))

    def draw(self, screen: LogicalScreen) -> None:
        w, h = self.size
        is_active = self.is_active
        border_attr = self.theme['twindow.border' if is_active else 'twindow.border.inactive']
        bg_attr = self.theme['twindow.background' if is_active else 'twindow.background.inactive']

        # Fiddle with clipping to draw shadow
        with screen.saving_context():
            screen.set_clip_right(screen.clip_right + 2)
            screen.set_clip_bottom(screen.clip_bottom + 1)
            screen.draw_box(0, 0, w, h, border_attr, bg_attr, 2 if is_active else 1, True)

        if self.title is not None and w > 4:
            title_str = self.title

            if stringutils.width(title_str) > w-4:
                # Title needs trimming to fit in available space.
                cells, title_w = [], 0
                for cell, cell_w in stringutils.str_cells(title_str):
                    if title_w + cell_w > w-4:
                        break
                    cells.append(cell)
                    title_w += cell_w
                title_str = ''.join(title_str)

            tx = (w - stringutils.width(title_str) - 2) >> 1
            assert tx > 0
            screen.put_string_x_y(tx, 0, f" {title_str} ", border_attr)

        # Draw children
        super().draw(screen)

    def on_mouse(self, event: TMouseEvent) -> bool:
        super().on_mouse(event)

        # A TWindow _always_ handles a mouse event directed to it to prevent widgets under it
        # getting the event.
        return True


class TWindowStack(TFill):
    """
    A TWindowStack maintains a stack of TWindow widgets and allows for re-positioning them. A
    TWindowStack is always active.

    """
    @property
    def is_active(self) -> bool:
        return True

    def set_active_child(self, child: TWidget) -> None:
        super().set_active_child(child)
        if isinstance(child, TWindow) and self.children[-1] is not child:
            self.raise_window(child)

    def add_window(self, window: TWindow) -> None:
        self.pack(window)
        if len(self.children) > 0 and self.active_child is not window:
            self.set_active_child(self.children[-1])

    def remove_window(self, window: TWindow) -> None:
        self.unpack(window)
        if len(self.children) > 0 and self.active_child is not self.children[-1]:
            self.set_active_child(self.children[-1])

    def raise_window(self, window: TWindow) -> None:
        self.unpack(window)
        self.pack(window)
        if len(self.children) > 0 and self.active_child is not window:
            self.set_active_child(self.children[-1])
