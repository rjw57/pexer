from setuptools import setup, find_packages

setup(
    name='pexer',
    version='2.0.0dev1',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'aioconsole~=0.3',
        'wcwidth~=0.2',
    ],
    package_data={
        "": [
            "*.txt",
            "*.md",
        ],
    },
)
